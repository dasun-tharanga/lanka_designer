
@if(session('status'))
    <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i>{{ session('status') }}</h4>
    </div>
@endif

@if($errors->any())
    <div class="clearfix">
        <div class="alert alert-danger" style="margin-bottom: 0!important;">
            <h4><i class="fa fa-info"></i> Note:</h4>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </div>
    </div>
    <br>
@endif