<section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>

        <li>
            <a href="/admin/">
                <i class="fa fa-files-o"></i>
                <span>Projects</span>
            </a>
        </li>
        <li>
            <a href="/admin/client/">
                <i class="fa  fa-users"></i> <span>Clients</span>
            </a>
        </li>
        <li class="treeview">
            <a href="#">
                <i class="fa fa-feed"></i>
                <span>Blog Posts</span>
            </a>
        </li>
        <li class="treeview">
            <a href="{{route('logout')}}"  onclick="event.preventDefault();
               document.getElementById('logout-form').submit();">
                <i class="fa fa-sign-out"></i>
                <span>Log out</span>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>

    </ul>
</section>