
<!DOCTYPE html>
<html lang="en-US">
<head>
<meta charset="UTF-8"/>
<meta name="twitter:widgets:csp" content="on"/>
<link rel="profile" href="//gmpg.org/xfn/11"/>
<link rel="pingback" href="/xmlrpc.php"/>

<title>Web Lankan | Digital Marketing Agency |  Web Design | Sri Lanka</title>

<!-- All in One SEO Pack Pro 2.5.4.1 by Michael Torbert of Semper Fi Web Design[243,316] -->
<!-- Debug String: 2JMJ7L5RSW0YVB/VLWAYKK/YBWK -->
<meta name="description"  content="Web Lankan is an ISO certified Digital marketing agency which delivers tailored services to any business entity in Digital marketing, web designing, and software development. We provide digital solutions to help you maximize the performance of the Digital Marketing &amp; Website Design objectives." />

<meta name="keywords"  content="web design sri lanka, web designers in sri lanka, web design in sri lanka, web designers sri lanka, sri lanka web design, web designing sri lanka, web design company sri lanka, Web hosting in sri lanka , web lanka, online marketing,Digital Marketing,Digital Marketing agency, Digital Marketing Sri Lanka,content marketing,social media,domain registration,android app development,Mobile apps" />

<link rel="canonical" href="/" />
			<script type="text/javascript" >
				window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
				ga('create', 'UA-122114860-1', 'auto');
				// Plugins

				ga('send', 'pageview');
			</script>
			<script async src="//www.google-analytics.com/analytics.js"></script>
			<!-- /all in one seo pack pro -->
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="Web Lankan &raquo; Feed" href="/feed/" />
<link rel="alternate" type="application/rss+xml" title="Web Lankan &raquo; Comments Feed" href="/comments/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.8.8"}};
			!function(a,b,c){function d(a){var b,c,d,e,f=String.fromCharCode;if(!k||!k.fillText)return!1;switch(k.clearRect(0,0,j.width,j.height),k.textBaseline="top",k.font="600 32px Arial",a){case"flag":return k.fillText(f(55356,56826,55356,56819),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,56826,8203,55356,56819),0,0),c=j.toDataURL(),b!==c&&(k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447),0,0),b=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447),0,0),c=j.toDataURL(),b!==c);case"emoji4":return k.fillText(f(55358,56794,8205,9794,65039),0,0),d=j.toDataURL(),k.clearRect(0,0,j.width,j.height),k.fillText(f(55358,56794,8203,9794,65039),0,0),e=j.toDataURL(),d!==e}return!1}function e(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g,h,i,j=b.createElement("canvas"),k=j.getContext&&j.getContext("2d");for(i=Array("flag","emoji4"),c.supports={everything:!0,everythingExceptFlag:!0},h=0;h<i.length;h++)c.supports[i[h]]=d(i[h]),c.supports.everything=c.supports.everything&&c.supports[i[h]],"flag"!==i[h]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[i[h]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='zn_all_g_fonts-css'  href='//fonts.googleapis.com/css?family=Montserrat%3Aregular%2C500%2C700%7COpen+Sans%3Aregular%2C300%2C600%2C700%2C800&#038;ver=4.8.8' type='text/css' media='all' />
<link rel='stylesheet' id='formcraft-common-css'  href='/wp-content/plugins/formcraft3/dist/formcraft-common.css?ver=3.6' type='text/css' media='all' />
<link rel='stylesheet' id='formcraft-form-css'  href='/wp-content/plugins/formcraft3/dist/form.css?ver=3.6' type='text/css' media='all' />
<link rel='stylesheet' id='rs-plugin-settings-css'  href='/wp-content/plugins/revslider/public/assets/css/settings.css?ver=5.4.5.1' type='text/css' media='all' />
<style id='rs-plugin-settings-inline-css' type='text/css'>
#rs-demo-id {}
</style>
<link rel='stylesheet' id='plhg-main-css-css'  href='/wp-content/plugins/hogash-post-love/assets/frontend/css/plhg-styles.css?ver=1.0.0' type='text/css' media='all' />
<link rel='stylesheet' id='kallyas-styles-css'  href='/wp-content/themes/weblankan/style.css?ver=4.15.2' type='text/css' media='all' />
<link rel='stylesheet' id='th-bootstrap-styles-css'  href='/wp-content/themes/weblankan/css/bootstrap.min.css?ver=4.15.2' type='text/css' media='all' />
<link rel='stylesheet' id='th-theme-template-styles-css'  href='/wp-content/themes/weblankan/css/template.min.css?ver=4.15.2' type='text/css' media='all' />
<link rel='stylesheet' id='tf-compiled-options-mobmenu-css'  href='/wp-content/uploads/titan-framework-mobmenu-css.css?ver=4.8.8' type='text/css' media='all' />
<link rel='stylesheet' id='tf-google-webfont-montserrat-css'  href='//fonts.googleapis.com/css?family=Montserrat%3Ainherit%2C400&#038;subset=latin%2Clatin-ext&#038;ver=4.8.8' type='text/css' media='all' />
<link rel='stylesheet' id='tf-google-webfont-dosis-css'  href='//fonts.googleapis.com/css?family=Dosis%3Ainherit%2C400&#038;subset=latin%2Clatin-ext&#038;ver=4.8.8' type='text/css' media='all' />
<link rel='stylesheet' id='zion-frontend-css'  href='/wp-content/themes/weblankan/framework/zion-builder/assets/css/znb_frontend.css?ver=1.0.1' type='text/css' media='all' />
<link rel='stylesheet' id='2428-layout.css-css'  href='/wp-content/uploads/zion-builder/cache/2428-layout.css?ver=1a86f4e24b80d8f807ce52d314e4aaa7' type='text/css' media='all' />
<link rel='stylesheet' id='kallyas-addon-nav-overlay-css-css'  href='/wp-content/plugins/kallyas-addon-nav-overlay/assets/styles.min.css?ver=1.0.10' type='text/css' media='all' />
<link rel='stylesheet' id='th-theme-print-stylesheet-css'  href='/wp-content/themes/weblankan/css/print.css?ver=4.15.2' type='text/css' media='print' />
<link rel='stylesheet' id='th-theme-options-styles-css'  href='/wp-content/uploads/zn_dynamic.css?ver=1547205264' type='text/css' media='all' />
<link rel='stylesheet' id='cssmobmenu-icons-css'  href='/wp-content/plugins/mobile-menu/includes/css/mobmenu-icons.css?ver=4.8.8' type='text/css' media='all' />
<script type='text/javascript' src='/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script type='text/javascript' src='/wp-includes/js/jquery.ripples.js?ver=4.8.8'></script>
<script type='text/javascript' src='/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.tools.min.js?ver=5.4.5.1'></script>
<script type='text/javascript' src='/wp-content/plugins/revslider/public/assets/js/jquery.themepunch.revolution.min.js?ver=5.4.5.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var plhg_script_vars = {"ajaxurl":"https:\/\/wp-admin\/admin-ajax.php","nonce":"54dbf72800","error_message":"Sorry, there was a problem processing your request."};
/* ]]> */
</script>
<script type='text/javascript' src='/wp-content/plugins/hogash-post-love/assets/frontend/js/script.js?ver=1.0.0'></script>
<script type='text/javascript' src='/wp-content/plugins/mobile-menu/includes/js/mobmenu.js?ver=4.8.8'></script>
<link rel='//api.w.org/' href='/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="/wp-includes/wlwmanifest.xml" />
<meta name="generator" content="WordPress 4.8.8" />
<link rel='shortlink' href='/' />
<link rel="alternate" type="application/json+oembed" href="/wp-json/oembed/1.0/embed?url=https%3A%2F%%2F" />
<link rel="alternate" type="text/xml+oembed" href="/wp-json/oembed/1.0/embed?url=https%3A%2F%%2F&#038;format=xml" />
		<meta name="theme-color"
			  content="#cd2122">
				<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

		<!--[if lte IE 8]>
		<script type="text/javascript">
			var $buoop = {
				vs: {i: 10, f: 25, o: 12.1, s: 7, n: 9}
			};

			$buoop.ol = window.onload;

			window.onload = function () {
				try {
					if ($buoop.ol) {
						$buoop.ol()
					}
				}
				catch (e) {
				}

				var e = document.createElement("script");
				e.setAttribute("type", "text/javascript");
				e.setAttribute("src", "//browser-update.org/update.js");
				document.body.appendChild(e);
			};
		</script>
		<![endif]-->

		<!-- for IE6-8 support of HTML5 elements -->
		<!--[if lt IE 9]>
		<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
				<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
		<meta name="generator" content="Powered by Slider Revolution 5.4.5.1 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />

<style>

/* Hide WP Mobile Menu outside the width of trigger */
@media only screen and (min-width:993px) {

	.mob_menu, .mob_menu_left_panel, .mob_menu_right_panel, .mobmenu {
		display: none!important;
	}

}

/* Our css Custom Options values */
@media only screen and (max-width:993px) {
	 {
		display:none !important;
	}

		.mob-menu-left-panel .mobmenu-left-bt, .mob-menu-right-panel .mobmenu-right-bt {
		position: absolute;
		right: 0px;
		top: 10px;
		font-size: 30px;
	}

	.mob-menu-slideout  .mob-cancel-button{
		display: none;
	}

	.mobmenu, .mob-menu-left-panel, .mob-menu-right-panel {
		display: block;
	}

	.mobmenur-container i {
		color: #222;
	}

	.mobmenul-container i {
		color: #222;
	}
	.mobmenul-container img {
		max-height:  70px;
		float: left;
	}
		.mobmenur-container img {
		max-height:  70px;
		float: right;
	}
	#mobmenuleft li a , #mobmenuleft li a:visited {
		color: #222;

	}
	.mobmenu_content h2, .mobmenu_content h3, .show-nav-left .mob-menu-copyright, .show-nav-left .mob-expand-submenu i {
		color: #222;
	}

	.mobmenu_content #mobmenuleft li:hover, .mobmenu_content #mobmenuright li:hover  {
		background-color: #a3d3e8;
	}

	.mobmenu_content #mobmenuright li:hover  {
		background-color: #031121;
	}

	.mobmenu_content #mobmenuleft .sub-menu {
		background-color: #eff1f1;
		margin: 0;
		color: #222;
		width: 100%;
		position: initial;
	}
	.mob-menu-left-panel .mob-cancel-button {
		color: #000;
	}
	.mob-menu-right-panel .mob-cancel-button {
		color: #000;
	}
	.mob-menu-slideout-over .mobmenu_content {
		padding-top: 40px;
	}

	.mob-menu-left-bg-holder {
				opacity: 1;
		background-attachment: fixed ;
		background-position: center top ;
		-webkit-background-size:  cover;
		-moz-background-size: cover;
		background-size: cover;
	}
	.mob-menu-right-bg-holder {
				opacity: 1;
		background-attachment: fixed ;
		background-position: center top ;
		-webkit-background-size: cover;
		-moz-background-size: cover;
		background-size:  cover;
	}

	.mobmenu_content #mobmenuleft .sub-menu a {
		color: #222;
	}

	.mobmenu_content #mobmenuright .sub-menu  a{
		color: #ffffff;
	}
	.mobmenu_content #mobmenuright .sub-menu .sub-menu {
		background-color: inherit;
	}

	.mobmenu_content #mobmenuright .sub-menu  {
		background-color: #2a4765;
		margin: 0;
		color: #ffffff ;
		position: initial;
		width: 100%;
	}

	#mobmenuleft li a:hover {
		color: #fff ;

	}

	#mobmenuright li a , #mobmenuright li a:visited, .show-nav-right .mob-menu-copyright, .show-nav-right .mob-expand-submenu i {
		color: #ffffff;
	}

	#mobmenuright li a:hover {
		color: #fff;
	}

	.mobmenul-container {
		top: 5px;
		margin-left: 5px;
	}

	.mobmenur-container {
		top: 5px;
		margin-right: 5px;
	}

	/* 2nd Level Menu Items Padding */
	.mobmenu .sub-menu li a {
		padding-left: 50px;
	}

	/* 3rd Level Menu Items Padding */
	.mobmenu .sub-menu .sub-menu li a {
		padding-left: 75px;
	}


	.mob-menu-logo-holder {
		padding-top: 10px;
		text-align: left;
		margin-left:10px;;
		;
	}

	.mob-menu-header-holder {

		background-color: #fbfbfb;
		height: 70px;
		width: 100%;
		font-weight:bold;
		position:fixed;
		top:0px;
		right: 0px;
		z-index: 99998;
		color:#000;
		display: block;
	}

	.mobmenu-push-wrap, body.mob-menu-slideout-over {
		padding-top: 70px;
	}
		.mob-menu-slideout 	.mob-menu-left-panel {
		background-color:#f9f9f9;;
		width: 270px;
		-webkit-transform: translateX(-270px);
		-moz-transform: translateX(-270px);
		-ms-transform: translateX(-270px);
		-o-transform: translateX(-270px);
		transform: translateX(-270px);
	}

	.mob-menu-slideout .mob-menu-right-panel {
		background-color:#072340;		width: 270px;
		-webkit-transform: translateX( 270px );
		-moz-transform: translateX( 270px );
		-ms-transform: translateX( 270px );
		-o-transform: translateX( 270px );
		transform: translateX( 270px );
	}

	/* Will animate the content to the right 275px revealing the hidden nav */
	.mob-menu-slideout.show-nav-left .mobmenu-push-wrap, .mob-menu-slideout.show-nav-left .mob-menu-header-holder {

		-webkit-transform: translate(270px, 0);
		-moz-transform: translate(270px, 0);
		-ms-transform: translate(270px, 0);
		-o-transform: translate(270px, 0);
		transform: translate(270px, 0);
		-webkit-transform: translate3d(270px, 0, 0);
		-moz-transform: translate3d(270px, 0, 0);
		-ms-transform: translate3d(270px, 0, 0);
		-o-transform: translate3d(270px, 0, 0);
		transform: translate3d(270px, 0, 0);
	}

	.mob-menu-slideout.show-nav-right .mobmenu-push-wrap , .mob-menu-slideout.show-nav-right .mob-menu-header-holder {

		-webkit-transform: translate(-270px, 0);
		-moz-transform: translate(-270px, 0);
		-ms-transform: translate(-270px, 0);
		-o-transform: translate(-270px, 0);
		transform: translate(-270px, 0);

		-webkit-transform: translate3d(-270px, 0, 0);
		-moz-transform: translate3d(-270px, 0, 0);
		-ms-transform: translate3d(-270px, 0, 0);
		-o-transform: translate3d(-270px, 0, 0);
		transform: translate3d(-270px, 0, 0);
	}


	/* Mobmenu Slide Over */
	.mobmenu-overlay {
		opacity: 0;
	}

	.mob-menu-slideout-top .mobmenu-overlay, .mob-menu-slideout .mob-menu-right-panel .mob-cancel-button, .mob-menu-slideout .mob-menu-left-panel .mob-cancel-button {
		display: none!important;
	}

	.show-nav-left .mobmenu-overlay, .show-nav-right .mobmenu-overlay {
		width: 100%;
		height: 100%;
		background: rgba(0, 0, 0, 0.83);
		z-index: 99999;
		position: absolute;
		left: 0;
		top: 0;
		opacity: 1;
		-webkit-transition: .5s ease;
		-moz-transition: .5s ease;
		-ms-transition: .5s ease;
		-o-transition: .5s ease;
		transition: .5s ease;
		position: fixed;
		cursor: pointer;
	}

	.mob-menu-slideout-over .mob-menu-left-panel {
		display: block!important;
		background-color:#f9f9f9;;
		width: 270px;
		-webkit-transform: translateX(-270px);
		-moz-transform: translateX(-270px);
		-ms-transform: translateX(-270px);
		-o-transform: translateX(-270px);
		transform: translateX(-270px);
		-webkit-transition: -webkit-transform .5s;
		-moz-transition: -moz-transform .5s;
		-ms-transition: -ms-transform .5s;
		-o-transition: -o-transform .5s;
		transition: transform .5s;
	}

	.mob-menu-slideout-over .mob-menu-right-panel {
		display: block!important;
		background-color:#072340;;
		width:  270px;
		-webkit-transform: translateX(270px);
		-moz-transform: translateX(270px);
		-ms-transform: translateX(270px);
		-o-transform: translateX(270px);
		transform: translateX(270px);
		-webkit-transition: -webkit-transform .5s;
		-moz-transition: -moz-transform .5s;
		-ms-transition: -ms-transform .5s;
		-o-transition: -o-transform .5s;
		transition: transform .5s;
	}

	.mob-menu-slideout-over.show-nav-left .mob-menu-left-panel {
		display: block!important;
		background-color:#f9f9f9;;
		width:  270px;
		-webkit-transform: translateX(0);
		-moz-transform: translateX(0);
		-ms-transform: translateX(0);
		-o-transform: translateX(0);
		transform: translateX(0);
		-webkit-transition: -webkit-transform .5s;
		-moz-transition: -moz-transform .5s;
		-ms-transition: -ms-transform .5s;
		-o-transition: -o-transform .5s;
		transition: transform .5s;
	}

	.show-nav-right.mob-menu-slideout-over .mob-menu-right-panel {
		display: block!important;
		background-color:#072340;		width:  270px;
		-webkit-transform: translateX( 0 );
		-moz-transform: translateX( 0 );
		-ms-transform: translateX( 0 );
		-o-transform: translateX(0 );
		transform: translateX( 0 );
	}

	/* Hides everything pushed outside of it */
	.mob-menu-slideout .mob-menu-left-panel, .mob-menu-slideout-over .mob-menu-left-panel  {
		position: fixed;
		top: 0;
		height: 100%;
		z-index: 300000;
		overflow-y: auto;
		overflow-x: hidden;
		opacity: 1;
	}


	.mob-menu-slideout .mob-menu-right-panel, .mob-menu-slideout-over .mob-menu-right-panel {
		position: fixed;
		top: 0;
		right: 0;
		height: 100%;
		z-index: 300000;
		overflow-y: auto;
		overflow-x: hidden;
		opacity: 1;

	}

	/*End of Mobmenu Slide Over */

	.mobmenu .headertext {
		color: #222;
	}

	.headertext span {
		position: absolute;
		line-height: 70px;
	}


	/* Adds a transition and the resting translate state */
	.mob-menu-slideout .mobmenu-push-wrap, .mob-menu-slideout .mob-menu-header-holder {

		-webkit-transition: -webkit-transform .5s;
		-moz-transition: -moz-transform .5s;
		-ms-transition: -ms-transform .5s;
		-o-transition: -o-transform .5s;
		transition: transform .5s;
		-webkit-transform: translate(0, 0);
		-moz-transform: translate(0, 0);
		-ms-transform: translate(0, 0);
		-o-transform: translate(0, 0);
		transform: translate(0, 0);
		-webkit-transform: translate3d(0, 0, 0);
		-moz-transform: translate3d(0, 0, 0);
		-ms-transform: translate3d(0, 0, 0);
		-o-transform: translate3d(0, 0, 0);
		transform: translate3d(0, 0, 0);

	}

	/* Mobile Menu Frontend CSS Style*/
	html, body {
		overflow-x: hidden;
	}

	.hidden-overflow {
		overflow: hidden!important;
	}

	/* Hides everything pushed outside of it */
	.mob-menu-slideout .mob-menu-left-panel {
		position: fixed;
		top: 0;
		height: 100%;
		z-index: 300000;
		overflow-y: auto;
		overflow-x: hidden;
		opacity: 1;
		-webkit-transition: -webkit-transform .5s;
		-moz-transition: -moz-transform .5s;
		-ms-transition: -ms-transform .5s;
		-o-transition: -o-transform .5s;
		transition: transform .5s;
	}

	.mob-menu-slideout.show-nav-left .mob-menu-left-panel {
		transition: transform .5s;
		-webkit-transform: translateX(0);
		-moz-transform: translateX(0);
		-ms-transform: translateX(0);
		-o-transform: translateX(0);
		transform: translateX(0);
	}

	body.admin-bar .mobmenu {
		top: 32px;
	}

	@media screen and ( max-width: 782px ){
		body.admin-bar .mobmenu {
			top: 46px;
		}
	}

	.mob-menu-slideout .mob-menu-right-panel {
		position: fixed;
		top: 0;
		right: 0;
		height: 100%;
		z-index: 300000;
		overflow-y: auto;
		overflow-x: hidden;
		opacity: 1;
		-webkit-transition: -webkit-transform .5s;
		-moz-transition: -moz-transform .5s;
		-ms-transition: -ms-transform .5s;
		-o-transition: -o-transform .5s;
		transition: transform .5s;
	}

	.mob-menu-slideout.show-nav-right .mob-menu-right-panel {
		transition: transform .5s;
		-webkit-transform: translateX(0);
		-moz-transform: translateX(0);
		-ms-transform: translateX(0);
		-o-transform: translateX(0);
		transform: translateX(0);
	}

	.show-nav-left .mobmenu-push-wrap {
		height: 100%;
	}

	/* Will animate the content to the right 275px revealing the hidden nav */
	.mob-menu-slideout.show-nav-left .mobmenu-push-wrap, .show-nav-left .mob-menu-header-holder {
		-webkit-transition: -webkit-transform .5s;
		-moz-transition: -moz-transform .5s;
		-ms-transition: -ms-transform .5s;
		-o-transition: -o-transform .5s;
		transition: transform .5s;
	}

	.show-nav-right .mobmenu-push-wrap {
		height: 100%;
	}

	/* Will animate the content to the right 275px revealing the hidden nav */
	.mob-menu-slideout.show-nav-right .mobmenu-push-wrap , .mob-menu-slideout.show-nav-right .mob-menu-header-holder{
		-webkit-transition: -webkit-transform .5s;
		-moz-transition: -moz-transform .5s;
		-ms-transition: -ms-transform .5s;
		-o-transition: -o-transform .5s;
		transition: transform .5s;
	}

	.widget img {
		max-width: 100%;
	}

	#mobmenuleft, #mobmenuright {
		margin: 0;
		padding: 0;
	}

	#mobmenuleft li > ul {
		display:none;
		left: 15px;
	}

	.mob-expand-submenu {
		position: relative;
		right: 0px;
		float: right;
		margin-top: -50px;
	}

	.mob-expand-submenu i {
		padding: 12px;
	}

	#mobmenuright  li > ul {
		display:none;
		left: 15px;
	}

	.rightmbottom, .rightmtop {
		padding-left: 10px;
		padding-right: 10px;
	}

	.mobmenu_content {
		z-index: 1;
		height: 100%;
		overflow: auto;
	}

	.mobmenu_content li a {
		display: block;
		letter-spacing: 1px;
		padding: 10px 20px;
		text-decoration: none;
	}

	.mobmenu_content li {
		list-style: none;
	}
	.mob-menu-left-panel li, .leftmbottom, .leftmtop{
		padding-left: 0%;
		padding-right: 0%;
	}

	.mob-menu-right-panel li, .rightmbottom, .rightmtop{
		padding-left: 0%;
		padding-right: 0%;
	}

	.mob-menu-slideout .mob_menu_left_panel_anim {
		-webkit-transition: all .30s ease-in-out !important;
		transition: all .30s ease-in-out !important;
		transform: translate(0px) !important;
		-ms-transform: translate(0px) !important;
		-webkit-transform: translate(0px) !important;
	}

	.mob-menu-slideout .mob_menu_right_panel_anim {
		-webkit-transition: all .30s ease-in-out !important;
		transition: all .30s ease-in-out !important;
		transform: translate(0px) !important;
		-ms-transform: translate(0px) !important;
		-webkit-transform: translate(0px) !important;
	}

	.mobmenul-container {
		position: absolute;
	}

	.mobmenur-container {
		position: absolute;
		right: 0px;
	}

	.mob-menu-slideout .mob_menu_left_panel {
		width: 230px;
		height: 100%;
		position: fixed;
		top: 0px;
		left: 0px;
		z-index: 99999999;
		transform: translate(-230px);
		-ms-transform: translate(-230px);
		-webkit-transform: translate(-230px);
		transition: all .30s ease-in-out !important;
		-webkit-transition: all .30s ease-in-out !important;
		overflow:hidden;
	}

	.leftmbottom h2 {
		font-weight: bold;
		background-color: transparent;
		color: inherit;
	}

	.show-nav-right .mobmenur-container img, .show-nav-left .mobmenul-container img,  .mobmenu .mob-cancel-button, .show-nav-left .mobmenu .mob-menu-icon, .show-nav-right .mobmenu .mob-menu-icon, .mob-menu-slideout-over.show-nav-left .mobmenur-container, .mob-menu-slideout-over.show-nav-right .mobmenul-container  {
		display:none;
	}

	.show-nav-left .mobmenu .mob-cancel-button,  .mobmenu .mob-menu-icon, .show-nav-right .mobmenu .mob-cancel-button {
		display:block;
	}

	.mobmenul-container i {
		line-height: 30px;
		font-size: 30px;
		float: left;
	}
	.left-menu-icon-text {
		float: left;
		line-height: 30px;
		color: #222;
	}

	.right-menu-icon-text {
		float: right;
		line-height: 30px;
		color: #ffffff;
	}

	.mobmenur-container i {
		line-height: 30px;
		font-size: 30px;
		float: right;
	}

	.mobmenu_content .widget {
		padding-bottom: 0px;
		padding: 20px;
	}

	.mobmenu input[type="text"]:focus, .mobmenu input[type="email"]:focus, .mobmenu textarea:focus, .mobmenu input[type="tel"]:focus, .mobmenu input[type="number"]:focus {
		border-color: rgba(0, 0, 0, 0)!important;
	}

	.mob-expand-submenu i {
		padding: 12px;
		top: 10px;
		position: relative;
		font-weight: 600;
		cursor: pointer;
	}

	.nav, .main-navigation, .genesis-nav-menu, #main-header, #et-top-navigation, .site-header, .site-branding, .ast-mobile-menu-buttons, .hide {
		display: none!important;
	}

	.mob-menu-left-bg-holder, .mob-menu-right-bg-holder {
		width: 100%;
		height: 100%;
		position: absolute;
		z-index: -50;
		background-repeat: no-repeat;
		top: 0;
		left: 0;
	}

	.mobmenu_content .sub-menu {
		display: none;
	}

	.mob-standard-logo {
		display: inline-block;
		height:50px!important;	}

	.mob-retina-logo {
		height:50px!important;	}
}
.mobmenu-push-wrap {
	height:100%;
}
.no-menu-assigned {
	font-size: 12px;
	padding-left: 10px;
	margin-top: 20px;
	position: absolute;
}

</style>

<!-- Generated inline styles --><style type='text/css' id='zn-inline-styles'>.znFlipboxElm.znFlipbox--shadowsHover .znFlipbox {
    box-shadow: 0 0 0 transparent;
    background-color: #085377ab;
}


/* Add negative top margin to Action Box */
.eluid91fd4acb {margin-top:-25px;}

.zn_contact_submit{
    border-radius: 3px !important;
    margin-top: -149px;
    margin-right: 6px;
}

.btn-group-sm>.btn, .btn.btn-sm{    padding: 10px 13px 10px;}



.znSmartCarousel-navPosition--top-left, .znSmartCarousel-navPosition--bottom-left {
    float: left;
    left: 135px;
    position: absolute;
    top: 80%;
}

.znSmartCarousel-navPosition--top-right, .znSmartCarousel-navPosition--bottom-right {
    float: right;
    position: absolute;
       right: 14px;
    top: -34px;
}

.eluide3b7aab4 .container.custom_width_perc {
    width: 100%;
    padding-left: 0px;
    padding-right: 0px;
}

#eluida1a18141 .grid-ibx__icon{    width: 64px;}
</style><link rel="icon" href="/wp-content/uploads/2018/03/favicon.png" sizes="32x32" />
<link rel="icon" href="/wp-content/uploads/2018/03/favicon.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="/wp-content/uploads/2018/03/favicon.png" />
<meta name="msapplication-TileImage" content="/wp-content/uploads/2018/03/favicon.png" />
<script type="text/javascript">function setREVStartSize(e){
				try{ var i=jQuery(window).width(),t=9999,r=0,n=0,l=0,f=0,s=0,h=0;
					if(e.responsiveLevels&&(jQuery.each(e.responsiveLevels,function(e,f){f>i&&(t=r=f,l=e),i>f&&f>r&&(r=f,n=e)}),t>r&&(l=n)),f=e.gridheight[l]||e.gridheight[0]||e.gridheight,s=e.gridwidth[l]||e.gridwidth[0]||e.gridwidth,h=i/s,h=h>1?1:h,f=Math.round(h*f),"fullscreen"==e.sliderLayout){var u=(e.c.width(),jQuery(window).height());if(void 0!=e.fullScreenOffsetContainer){var c=e.fullScreenOffsetContainer.split(",");if (c) jQuery.each(c,function(e,i){u=jQuery(i).length>0?u-jQuery(i).outerHeight(!0):u}),e.fullScreenOffset.split("%").length>1&&void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0?u-=jQuery(window).height()*parseInt(e.fullScreenOffset,0)/100:void 0!=e.fullScreenOffset&&e.fullScreenOffset.length>0&&(u-=parseInt(e.fullScreenOffset,0))}f=u}else void 0!=e.minHeight&&f<e.minHeight&&(f=e.minHeight);e.c.closest(".rev_slider_wrapper").css({height:f})
				}catch(d){console.log("Failure at Presize of Slider:"+d)}
			};</script>
<script type="text/javascript">
_linkedin_partner_id = "295467";
window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
window._linkedin_data_partner_ids.push(_linkedin_partner_id);
</script><script type="text/javascript">
(function(){var s = document.getElementsByTagName("script")[0];
var b = document.createElement("script");
b.type = "text/javascript";b.async = true;
b.src = "//snap.licdn.com/li.lms-analytics/insight.min.js";
s.parentNode.insertBefore(b, s);})();
</script>
<noscript>
<img height="1" width="1" style="display:none;" alt="" src="//dc.ads.linkedin.com/collect/?pid=295467&fmt=gif" />
</noscript>


<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'//connect.facebook.net/en_US/fbevents.js');
fbq('init', '218507632315789');
fbq('track', 'PageView');
</script>
<noscript>
<img height="1" width="1"
src="//www.facebook.com/tr?id=218507632315789&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="//www.googletagmanager.com/gtag/js?id=UA-122114860-1"></script>

<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-122114860-1');
</script>

<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-4943885147982920",
    enable_page_level_ads: true
  });
</script>

</head>

<body  class="home page-template-default page page-id-2428 kl-skin--light mob-menu-slideout-over" itemscope="itemscope" itemtype="//schema.org/WebPage" >


<div class="login_register_stuff"></div><!-- end login register stuff -->		<div id="fb-root"></div>
		<script>(function (d, s, id) {
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) return;
				js = d.createElement(s);
				js.id = id;
				js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
				fjs.parentNode.insertBefore(js, fjs);
			}(document, 'script', 'facebook-jssdk'));</script>

<div id="zn-nav-overlay" class="znNavOvr znNavOvr--layoutS3 znNavOvr--animation2 znNavOvr--theme-light">

	<div class="znNavOvr-inner ">

		<div class="znNavOvr-s3-left"><div class="hidden-lg"></div><div class="znNavOvr-menuWrapper"></div></div><div class="znNavOvr-s3-right"><div class="znNavOvr-s3-rightTop"><div class="visible-lg"></div></div><div class="znNavOvr-s3-rightBottom"></div></div>
	</div>

	<a href="#" class="znNavOvr-close znNavOvr-close--trSmall" id="znNavOvr-close">
		<span></span>
		<svg x="0px" y="0px" width="54px" height="54px" viewBox="0 0 54 54">
			<circle fill="transparent" stroke="#656e79" stroke-width="1" cx="27" cy="27" r="25" stroke-dasharray="157 157" stroke-dashoffset="157"></circle>
		</svg>
	</a>
</div>
<div class="header-iso"><img class="header-iso-img" src="/wp-content/uploads/2018/07/iso-9001-2015.png" alt="" title="iso-weblankan"></div>
<div id="page_wrapper">

<header id="header" class="site-header  style2 cta_button    header--no-stick  sticky-resize headerstyle--default site-header--absolute nav-th--light siteheader-classic siteheader-classic-split sheader-sh--light"  >
		<div class="site-header-wrapper sticky-top-area">

		<div class="kl-top-header site-header-main-wrapper clearfix   sh--light">

		<div class="container siteheader-container header--oldstyles" style="width: 100%;padding-left: 0px;padding-right: 0px;">

				<div class="fxb-row fxb-row-col-sm">

										<div class='fxb-col fxb fxb-center-x fxb-center-y fxb-basis-auto fxb-grow-0'>
								<div id="logo-container" class="logo-container   logosize--no zn-original-logo">
			<!-- Logo -->
			<h1 class='site-logo logo ' id='logo'><a href='/' class='site-logo-anch'><img class="logo-img site-logo-img" src="/wp-content/uploads/2018/02/weblankan-web-logo.svg" width="195" height="63"  alt="Web Lankan" title="Digital Marketing Agency | Web Design Sri Lanka"  /></a></h1>			<!-- InfoCard -->
					</div>

							</div>

					<div class='fxb-col fxb-basis-auto'>



	<div class="separator site-header-separator visible-xs"></div>
	<div class="fxb-row site-header-row site-header-top ">

		<div class='fxb-col fxb fxb-start-x fxb-center-y fxb-basis-auto site-header-col-left site-header-top-left'>
			<div class="sh-component kl-header-toptext kl-font-alt">QUESTIONS? CALL: <a href="tel:+94112802280" class="fw-bold"> (+94) 112 80 22 80</a></div>					</div>

		<div class='fxb-col fxb fxb-end-x fxb-center-y fxb-basis-auto site-header-col-right site-header-top-right'>
						<div class="sh-component zn_header_top_nav-wrapper "><span class="headernav-trigger js-toggle-class" data-target=".zn_header_top_nav-wrapper" data-target-class="is-opened"></span><ul id="menu-top-bar-menu-1" class="zn_header_top_nav topnav topnav-no-sc clearfix"><li class="home-menu menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-2428 current_page_item menu-item-3247 active"><a href="/">Home</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2833"><a href="/about-us/">About Web Lankan</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4152"><a href="/our-works/">Our Works</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4479"><a href="/careers/">Careers</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2846"><a href="/contact-us/">Contact Us</a></li>
</ul></div><ul class="sh-component social-icons sc--clean topnav navRight topnav-no-hdnav"><li class="topnav-li social-icons-li"><a href="//www.facebook.com/weblankan" data-zniconfam="kl-social-icons" data-zn_icon="" target="_self" class="topnav-item social-icons-item scheader-icon-" title="Facebook"></a></li><li class="topnav-li social-icons-li"><a href="//www.linkedin.com/company/web-lankan" data-zniconfam="kl-social-icons" data-zn_icon="" target="_self" class="topnav-item social-icons-item scheader-icon-" title="Linkedin"></a></li></ul>		</div>

	</div><!-- /.site-header-top -->

	<div class="separator site-header-separator visible-xs"></div>


<div class="fxb-row site-header-row site-header-main ">

	<div class='fxb-col fxb fxb-start-x fxb-center-y fxb-basis-auto site-header-col-left site-header-main-left'>
			</div>

	<div class='fxb-col fxb fxb-center-x fxb-center-y fxb-basis-auto site-header-col-center site-header-main-center'>
			</div>

	<div class='fxb-col fxb fxb-end-x fxb-center-y fxb-basis-auto site-header-col-right site-header-main-right'>

		<div class='fxb-col fxb fxb-end-x fxb-center-y fxb-basis-auto site-header-main-right-top'>
								<div class="sh-component main-menu-wrapper" role="navigation" itemscope="itemscope" itemtype="//schema.org/SiteNavigationElement" >

					<div class="zn-res-menuwrapper">
			<a href="#" class="zn-res-trigger zn-menuBurger zn-menuBurger--4--m zn-menuBurger--anim2 " id="zn-res-trigger">
				<span></span>
				<span></span>
				<span></span>
			</a>
		</div><!-- end responsive menu -->
		<div id="main-menu" class="main-nav mainnav--overlay mainnav--active-bg mainnav--pointer-dash nav-mm--light zn_mega_wrapper "><ul id="menu-web-lankan-main-header-menu" class="main-menu main-menu-nav zn_mega_menu "><li id="menu-item-3613" class="main-menu-item menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-2428 current_page_item menu-item-3613 active  main-menu-item-top  menu-item-even menu-item-depth-0"><a href="/" class=" main-menu-link main-menu-link-top"><span>Home</span><span class="zn-mega-new-item"><img src="/wp-content/uploads/2018/07/web-page-home-1.png"></span></a></li>
<li id="menu-item-2958" class="main-menu-item menu-item menu-item-type-post_type menu-item-object-page menu-item-2958  main-menu-item-top  menu-item-even menu-item-depth-0"><a href="/web-design-development/" class=" main-menu-link main-menu-link-top"><span>Web Design &#038;<br>Development</span></a></li>
<li id="menu-item-2959" class="main-menu-item menu-item menu-item-type-post_type menu-item-object-page menu-item-2959  main-menu-item-top  menu-item-even menu-item-depth-0"><a href="/ecommerce-solutions/" class=" main-menu-link main-menu-link-top"><span>eCommerce<br>Solutions</span></a></li>
<li id="menu-item-2960" class="main-menu-item menu-item menu-item-type-post_type menu-item-object-page menu-item-2960  main-menu-item-top  menu-item-even menu-item-depth-0"><a href="/digital-marketing/" class=" main-menu-link main-menu-link-top"><span>Digital<br>Marketing</span></a></li>
<li id="menu-item-2961" class="main-menu-item menu-item menu-item-type-post_type menu-item-object-page menu-item-2961  main-menu-item-top  menu-item-even menu-item-depth-0"><a href="/seo-analytics/" class=" main-menu-link main-menu-link-top"><span>SEO &#038;<br>Analytics</span></a></li>
<li id="menu-item-2962" class="main-menu-item menu-item menu-item-type-post_type menu-item-object-page menu-item-2962  main-menu-item-top  menu-item-even menu-item-depth-0"><a href="/hosting-domain/" class=" main-menu-link main-menu-link-top"><span>Hosting<br>&#038; Domain</span></a></li>
<li id="menu-item-2963" class="main-menu-item menu-item menu-item-type-post_type menu-item-object-page menu-item-2963  main-menu-item-top  menu-item-even menu-item-depth-0"><a href="/content-development/" class=" main-menu-link main-menu-link-top"><span>Content<br>Development</span></a></li>
<li id="menu-item-3241" class="main-menu-item menu-item menu-item-type-post_type menu-item-object-page menu-item-3241  main-menu-item-top  menu-item-even menu-item-depth-0"><a href="/logo-design-branding/" class=" main-menu-link main-menu-link-top"><span>Logo Design<br> &#038; Branding</span></a></li>
<li id="menu-item-2964" class="main-menu-item menu-item menu-item-type-post_type menu-item-object-page menu-item-2964  main-menu-item-top  menu-item-even menu-item-depth-0"><a href="/professional-videophotography/" class=" main-menu-link main-menu-link-top"><span>Professional<br>Video/photography</span></a></li>
<li id="menu-item-3238" class="main-menu-item menu-item menu-item-type-post_type menu-item-object-page menu-item-3238  main-menu-item-top  menu-item-even menu-item-depth-0"><a href="/software-mobile-solutions/" class=" main-menu-link main-menu-link-top"><span>Software &#038; <br>Mobile Solutions</span></a></li>
</ul></div>		</div>
		<!-- end main_menu -->
		<a href="/contact-us/"  id="ctabutton"  class="sh-component ctabutton kl-cta-ribbon "  target="_self" itemprop="url" >CONTACT US<svg version="1.1" class="trisvg" xmlns="//www.w3.org/2000/svg" xmlns:xlink="//www.w3.org/1999/xlink" x="0px" y="0px" preserveAspectRatio="none" width="14px" height="5px" viewBox="0 0 14.017 5.006" enable-background="new 0 0 14.017 5.006" xml:space="preserve"><path fill-rule="evenodd" clip-rule="evenodd" d="M14.016,0L7.008,5.006L0,0H14.016z"></path></svg></a>		</div>


	</div>

</div><!-- /.site-header-main -->


					</div>
				</div>
							</div><!-- /.siteheader-container -->
		</div><!-- /.site-header-main-wrapper -->

	</div><!-- /.site-header-wrapper -->
	</header>
<div class="zn_pb_wrapper clearfix zn_sortable_content" data-droplevel="0"><div class="eluid8a591389 " >		<div class="kl-slideshow 0 kl-revolution-slider portfolio_devices eluidfa793592  " >
			<div class="bgback"></div>

			<link href="//fonts.googleapis.com/css?family=Montserrat:700%2C400%2C800%2C300%7CRoboto:500" rel="stylesheet" property="stylesheet" type="text/css" media="all">
<div id="rev_slider_4_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
<!-- START REVOLUTION SLIDER 5.4.5.1 fullwidth mode -->
	<div id="rev_slider_4_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.5.1">
<ul>	<!-- SLIDE  -->
	<li data-index="rs-19" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="/wp-content/uploads/2018/09/Slider-01-bg-100x50.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
		<!-- MAIN IMAGE -->
		<img src="/wp-content/uploads/2018/09/Slider-01-bg.jpg"  alt="" title="Slider-01-bg"  width="1903" height="634" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
		<!-- LAYERS -->

		<!-- LAYER NR. 1 -->
		<div class="tp-caption   tp-resizeme"
			 id="slide-19-layer-1"
			 data-x="['left','left','left','left']" data-hoffset="['850','850','468','210']"
			 data-y="['top','top','top','top']" data-voffset="['114','114','362','280']"
						data-width="none"
			data-height="none"
			data-whitespace="nowrap"

			data-type="image"
			data-responsive_offset="on"

			data-frames='[{"delay":0,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 5;"><img src="/wp-content/uploads/2018/09/responsive-web-design.png" alt="" data-ww="['966px','966px','292px','244px']" data-hh="['439px','439px','133px','111px']" width="966" height="439" data-no-retina> </div>

		<!-- LAYER NR. 2 -->
		<div class="tp-caption   tp-resizeme"
			 id="slide-19-layer-3"
			 data-x="['left','left','left','left']" data-hoffset="['198','198','55','55']"
			 data-y="['top','top','top','top']" data-voffset="['206','206','124','124']"
						data-fontsize="['40','40','40','25']"
			data-lineheight="['45','45','45','30']"
			data-width="none"
			data-height="none"
			data-whitespace="nowrap"

			data-type="text"
			data-responsive_offset="on"

			data-frames='[{"delay":0,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 6; white-space: nowrap; font-size: 40px; line-height: 45px; font-weight: 700; color: #474747; letter-spacing: 0px;font-family:Montserrat;text-transform:uppercase;">Responsive Web Design </div>

		<!-- LAYER NR. 3 -->
		<div class="tp-caption   tp-resizeme"
			 id="slide-19-layer-4"
			 data-x="['left','left','left','left']" data-hoffset="['200','200','52','50']"
			 data-y="['top','top','top','top']" data-voffset="['267','267','184','162']"
						data-fontsize="['20','20','20','15']"
			data-lineheight="['35','35','35','20']"
			data-width="none"
			data-height="none"
			data-whitespace="nowrap"

			data-type="text"
			data-responsive_offset="on"

			data-frames='[{"delay":0,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 7; white-space: nowrap; font-size: 20px; line-height: 35px; font-weight: 400; color: #686868; letter-spacing: 0px;font-family:Montserrat;">- Responsive across different screen sizes<br>
- Visually appealing and functional web designs<br>
- Streamlined interfaces for optimized user experience <br>
- Uniquely designs with cutting-edge features<br>
- Practical web solutions to take you to the next level<br> </div>

		<!-- LAYER NR. 4 -->
		<div class="tp-caption rev-btn "
			 id="slide-19-layer-7"
			 data-x="['left','left','left','left']" data-hoffset="['207','207','207','207']"
			 data-y="['top','top','top','top']" data-voffset="['473','473','473','473']"
						data-width="none"
			data-height="none"
			data-whitespace="nowrap"

			data-type="button"
			data-actions='[{"event":"click","action":"simplelink","target":"_self","url":"https:\/\/contact-us\/","delay":""}]'
			data-responsive_offset="on"
			data-responsive="off"
			data-frames='[{"delay":0,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0,0,0,1);bg:rgba(255,255,255,1);bs:solid;bw:0 0 0 0;"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[10,10,10,10]"
			data-paddingright="[30,30,30,30]"
			data-paddingbottom="[10,10,10,10]"
			data-paddingleft="[30,30,30,30]"

			style="z-index: 8; white-space: nowrap; font-size: 14px; line-height: 14px; font-weight: 500; color: rgba(255,255,255,1); letter-spacing: px;font-family:Roboto;text-transform:uppercase;background-color:rgba(0,0,0,0.75);border-color:rgba(0,0,0,1);outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Get Started Now </div>
	</li>
	<!-- SLIDE  -->
	<li data-index="rs-17" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb=""  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
		<!-- MAIN IMAGE -->
		<img src="/wp-content/plugins/revslider/admin/assets/images/transparent.png" data-bgcolor='#ffffff' style='background:#ffffff' alt="Digital-Marketing-web lankan" title="Digital-Marketing-web lankan"  width="1903" height="634" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
		<!-- LAYERS -->

		<!-- LAYER NR. 5 -->
		<div class="tp-caption   tp-resizeme"
			 id="slide-17-layer-1"
			 data-x="['left','left','left','left']" data-hoffset="['323','323','20','143']"
			 data-y="['top','top','top','top']" data-voffset="['144','144','65','20']"
						data-width="none"
			data-height="none"
			data-whitespace="nowrap"

			data-type="image"
			data-responsive_offset="on"

			data-frames='[{"delay":0,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 5;"><img src="/wp-content/uploads/2018/09/digital-marketing.png" alt="" data-ww="['480','480','377px','182px']" data-hh="['558','558','437px','211px']" width="480" height="558" data-no-retina> </div>

		<!-- LAYER NR. 6 -->
		<div class="tp-caption   tp-resizeme"
			 id="slide-17-layer-2"
			 data-x="['left','left','left','left']" data-hoffset="['856','856','326','24']"
			 data-y="['top','top','top','top']" data-voffset="['134','134','225','153']"
						data-width="none"
			data-height="none"
			data-whitespace="nowrap"

			data-type="image"
			data-responsive_offset="on"

			data-frames='[{"delay":0,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 6;"><img src="/wp-content/uploads/2018/07/1-text.png" alt="Digital-marketing-agency" data-ww="['743px','743px','438px','438px']" data-hh="['426px','426px','251px','251px']" width="743" height="426" data-no-retina> </div>
	</li>
	<!-- SLIDE  -->
	<li data-index="rs-20" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-link="/ecommerce-solutions/"   data-thumb=""  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
		<!-- MAIN IMAGE -->
		<img src="/wp-content/plugins/revslider/admin/assets/images/transparent.png" data-bgcolor='#ffffff' style='background:#ffffff' alt="" title="Home"  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
		<!-- LAYERS -->

		<!-- LAYER NR. 7 -->
		<div class="tp-caption   tp-resizeme"
			 id="slide-20-layer-1"
			 data-x="['center','center','center','center']" data-hoffset="['3','3','-17','3']"
			 data-y="['bottom','bottom','bottom','bottom']" data-voffset="['38','38','32','78']"
						data-width="none"
			data-height="none"
			data-whitespace="nowrap"

			data-type="image"
			data-responsive_offset="on"

			data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 5;"><img src="/wp-content/uploads/2018/09/eCommerce.png" alt="" data-ww="['1903px','1903px','890px','577px']" data-hh="['634px','634px','296px','192px']" width="1903" height="634" data-no-retina> </div>

		<!-- LAYER NR. 8 -->
		<div class="tp-caption   tp-resizeme"
			 id="slide-20-layer-2"
			 data-x="['center','center','center','center']" data-hoffset="['0','0','0','1']"
			 data-y="['top','top','top','top']" data-voffset="['88','88','98','66']"
						data-fontsize="['30','30','21','14']"
			data-lineheight="['35','35','35','20']"
			data-width="none"
			data-height="none"
			data-whitespace="nowrap"

			data-type="text"
			data-responsive_offset="on"

			data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 6; white-space: nowrap; font-size: 30px; line-height: 35px; font-weight: 800; color: #595959; letter-spacing: 0px;font-family:Montserrat;text-transform:uppercase;">Use an online platform to increase your sales<br>
 </div>

		<!-- LAYER NR. 9 -->
		<div class="tp-caption   tp-resizeme"
			 id="slide-20-layer-3"
			 data-x="['center','center','center','center']" data-hoffset="['0','0','0','1']"
			 data-y="['top','top','top','top']" data-voffset="['126','126','126','88']"
						data-fontsize="['45','45','27','15']"
			data-lineheight="['50','50','50','20']"
			data-width="none"
			data-height="none"
			data-whitespace="nowrap"

			data-type="text"
			data-responsive_offset="on"

			data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 7; white-space: nowrap; font-size: 45px; line-height: 50px; font-weight: 300; color: #595959; letter-spacing: 0px;font-family:Montserrat;text-transform:uppercase;">Generate more traffic and boost your profit </div>

		<!-- LAYER NR. 10 -->
		<div class="tp-caption rev-btn "
			 id="slide-20-layer-5"
			 data-x="['left','left','left','left']" data-hoffset="['741','741','741','741']"
			 data-y="['top','top','top','top']" data-voffset="['248','248','248','248']"
						data-width="none"
			data-height="none"
			data-whitespace="nowrap"

			data-type="button"
			data-actions='[{"event":"click","action":"simplelink","target":"_self","url":"https:\/\/ecommerce-solutions\/","delay":""}]'
			data-responsive_offset="on"
			data-responsive="off"
			data-frames='[{"delay":0,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(0,0,0,1);bg:rgba(255,255,255,1);bs:solid;bw:0 0 0 0;"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[10,10,10,10]"
			data-paddingright="[30,30,30,30]"
			data-paddingbottom="[10,10,10,10]"
			data-paddingleft="[30,30,30,30]"

			style="z-index: 8; white-space: nowrap; font-size: 14px; line-height: 14px; font-weight: 500; color: rgba(255,255,255,1); letter-spacing: px;font-family:Roboto;text-transform:uppercase;background-color:rgba(0,0,0,0.75);border-color:rgba(0,0,0,1);outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Get Started Now </div>
	</li>
	<!-- SLIDE  -->
	<li data-index="rs-18" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-thumb="/wp-content/uploads/2018/07/bg-100x50.jpg"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
		<!-- MAIN IMAGE -->
		<img src="/wp-content/uploads/2018/07/bg.jpg"  alt="" title="bg"  width="1920" height="700" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
		<!-- LAYERS -->

		<!-- LAYER NR. 11 -->
		<div class="tp-caption   tp-resizeme"
			 id="slide-18-layer-1"
			 data-x="['center','center','center','center']" data-hoffset="['0','0','-7','2']"
			 data-y="['middle','middle','middle','middle']" data-voffset="['0','0','8','-6']"
						data-width="none"
			data-height="none"
			data-whitespace="nowrap"

			data-type="image"
			data-responsive_offset="on"

			data-frames='[{"delay":0,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
			data-textAlign="['inherit','inherit','inherit','inherit']"
			data-paddingtop="[0,0,0,0]"
			data-paddingright="[0,0,0,0]"
			data-paddingbottom="[0,0,0,0]"
			data-paddingleft="[0,0,0,0]"

			style="z-index: 5;"><img src="/wp-content/uploads/2018/07/text.png" alt="Awards- best developer-2018-Bestweb.lk" data-ww="['1184px','1184px','649px','462px']" data-hh="['480px','480px','263px','187px']" width="1184" height="480" data-no-retina> </div>
	</li>
</ul>
<script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css"); var htmlDivCss="";
						if(htmlDiv) {
							htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
						}else{
							var htmlDiv = document.createElement("div");
							htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
							document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
						}
					</script>
<div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>	</div>
<script>var htmlDiv = document.getElementById("rs-plugin-settings-inline-css"); var htmlDivCss="";
				if(htmlDiv) {
					htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
				}else{
					var htmlDiv = document.createElement("div");
					htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
					document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
				}
			</script>
		<script type="text/javascript">
setREVStartSize({c: jQuery('#rev_slider_4_1'), responsiveLevels: [1240,1240,778,480], gridwidth: [1920,1920,778,480], gridheight: [700,700,500,400], sliderLayout: 'fullwidth'});

var revapi4,
	tpj=jQuery;

tpj(document).ready(function() {
	if(tpj("#rev_slider_4_1").revolution == undefined){
		revslider_showDoubleJqueryError("#rev_slider_4_1");
	}else{
		revapi4 = tpj("#rev_slider_4_1").show().revolution({
			sliderType:"standard",
			jsFileLocation:"/wp-content/plugins/revslider/public/assets/js/",
			sliderLayout:"fullwidth",
			dottedOverlay:"none",
			delay:9000,
			navigation: {
				keyboardNavigation:"off",
				keyboard_direction: "horizontal",
				mouseScrollNavigation:"off",
 							mouseScrollReverse:"default",
				onHoverStop:"off",
				arrows: {
					style:"metis",
					enable:true,
					hide_onmobile:true,
					hide_under:768,
					hide_onleave:false,
					tmp:'',
					left: {
						h_align:"left",
						v_align:"center",
						h_offset:20,
						v_offset:0
					},
					right: {
						h_align:"right",
						v_align:"center",
						h_offset:20,
						v_offset:0
					}
				}
			},
			responsiveLevels:[1240,1240,778,480],
			visibilityLevels:[1240,1240,778,480],
			gridwidth:[1920,1920,778,480],
			gridheight:[700,700,500,400],
			lazyType:"none",
			shadow:0,
			spinner:"spinner0",
			stopLoop:"off",
			stopAfterLoops:-1,
			stopAtSlide:-1,
			shuffle:"off",
			autoHeight:"off",
			disableProgressBar:"on",
			hideThumbsOnMobile:"off",
			hideSliderAtLimit:0,
			hideCaptionAtLimit:0,
			hideAllCaptionAtLilmit:0,
			debugMode:false,
			fallbacks: {
				simplifyAll:"off",
				nextSlideOnWindowFocus:"off",
				disableFocusListener:false,
			}
		});
	}

});	/*ready*/
</script>
		<script>
					var htmlDivCss = unescape("%23rev_slider_4_1%20.metis.tparrows%20%7B%0A%20%20background%3Argba%28255%2C%20255%2C%20255%2C%201%29%3B%0A%20%20padding%3A10px%3B%0A%20%20transition%3Aall%200.3s%3B%0A%20%20-webkit-transition%3Aall%200.3s%3B%0A%20%20width%3A60px%3B%0A%20%20height%3A60px%3B%0A%20%20box-sizing%3Aborder-box%3B%0A%20%7D%0A%20%0A%20%23rev_slider_4_1%20.metis.tparrows%3Ahover%20%7B%0A%20%20%20background%3Argba%28255%2C255%2C255%2C0.75%29%3B%0A%20%7D%0A%20%0A%20%23rev_slider_4_1%20.metis.tparrows%3Abefore%20%7B%0A%20%20color%3Argb%280%2C%200%2C%200%29%3B%20%20%0A%20%20%20transition%3Aall%200.3s%3B%0A%20%20-webkit-transition%3Aall%200.3s%3B%0A%20%7D%0A%20%0A%20%23rev_slider_4_1%20.metis.tparrows%3Ahover%3Abefore%20%7B%0A%20%20%20transform%3Ascale%281.5%29%3B%0A%20%20%7D%0A%20%0A");
					var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
					if(htmlDiv) {
						htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
					}
					else{
						var htmlDiv = document.createElement('div');
						htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
						document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
					}
				  </script>
				</div><!-- END REVOLUTION SLIDER -->
			<div class="th-sparkles"></div>

			<div class="zn_header_bottom_style"></div>		</div>
		</div>		<section class="zn_section eluidf322c22a  blue-bggradiant   section-sidemargins    section--no " id="water"  >


			<div class="zn_section_size container zn-section-height--custom_height zn-section-content_algn--middle ">

				<div class="row gutter-0">

		<div class="eluid9639fcdf      col-md-12 col-sm-12   znColumnElement"  id="eluid9639fcdf" >


			<div class="znColumnElement-innerWrapper-eluid9639fcdf znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="kl-title-block clearfix tbk--text- tbk--center text-center tbk-symbol-- tbk--colored tbk-icon-pos--after-title eluid1657e714 " ><h1 class="tbk__title" itemprop="headline" >Professional consultations <br>for  <span style="
    color: #f33;
">online solutions</span>

</h1><h4 class="tbk__subtitle" itemprop="alternativeHeadline" >built with a smart and effective strategy ensuring real, measurable results for your business</h4></div>				</div>
			</div>


		</div>

		<div class="eluid37518e08      col-md-3 col-sm-3   znColumnElement"  id="eluid37518e08" >


			<div class="znColumnElement-innerWrapper-eluid37518e08 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">

		<div class="znFlipboxElm eluideec69358  znFlipbox--shadowsHover znFlipbox--rotateX znFlipbox--hAlignCenter znFlipbox--vAlignMiddle znFlipbox--speedNormal znFlipboxElm-pbOff" >

			<div class="znFlipbox znFlipbox-front"><div class="znFlipbox-content znFlipbox-contentFront"><div class="row zn_columns_container zn_content zn_col_container-flipbox" data-droplevel="1">
		<div class="eluid878ee10d      col-md-12 col-sm-12   znColumnElement"  id="eluid878ee10d" >


			<div class="znColumnElement-innerWrapper-eluid878ee10d znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="kl-iconbox eluid29175ed1   kl-iconbox--type-img   kl-iconbox--align-center text-center kl-iconbox--theme-light element-scheme--light"  id="eluid29175ed1">
	<div class="kl-iconbox__inner clearfix">


				<div class="kl-iconbox__icon-wrapper ">
			<img class="kl-iconbox__icon" src="/wp-content/uploads/2019/01/web-design-and-development.svg"  alt="" title="web-design-and-development">		</div><!-- /.kl-iconbox__icon-wrapper -->


		<div class="kl-iconbox__content-wrapper">




		</div><!-- /.kl-iconbox__content-wrapper -->

	</div>
</div>

<div class="kl-title-block clearfix tbk--text- tbk--center text-center tbk-symbol--  tbk-icon-pos--after-title eluidcb5fef01 " ><h3 class="tbk__title" itemprop="headline" >WEB</h3><h4 class="tbk__subtitle" itemprop="alternativeHeadline" >Web Development</h4></div>				</div>
			</div>


		</div>
	</div></div><div class="znFlipbox-overlayFront"></div></div><div class="znFlipbox znFlipbox-back"><div class="znFlipbox-content znFlipbox-contentBack"><div class="row zn_columns_container zn_content zn_col_container-flipbox" data-droplevel="1">
		<div class="eluidfcf6c546      col-md-12 col-sm-12   znColumnElement"  id="eluidfcf6c546" >


			<div class="znColumnElement-innerWrapper-eluidfcf6c546 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-center znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="kl-iconbox eluid8d6db8c0   kl-iconbox--type-icon   kl-iconbox--align-center text-center kl-iconbox--theme-light element-scheme--light"  id="eluid8d6db8c0">
	<div class="kl-iconbox__inner clearfix">


				<div class="kl-iconbox__icon-wrapper ">
			<span class="kl-iconbox__icon kl-iconbox__icon--" data-zniconfam="icomoon" data-zn_icon=""></span>		</div><!-- /.kl-iconbox__icon-wrapper -->


		<div class="kl-iconbox__content-wrapper">


						<div class=" kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
				<p class="kl-iconbox__desc">We employ the latest technology blended with creative ideas to add an outstanding quality to your website</p>
			</div>


		</div><!-- /.kl-iconbox__content-wrapper -->

	</div>
</div>

				</div>
			</div>


		</div>
	</div></div><div class="znFlipbox-overlayBack"></div></div>
		</div>

						</div>
			</div>


		</div>

		<div class="eluid283cb943      col-md-3 col-sm-3   znColumnElement"  id="eluid283cb943" >


			<div class="znColumnElement-innerWrapper-eluid283cb943 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">

		<div class="znFlipboxElm eluid90cd7313  znFlipbox--shadowsHover znFlipbox--rotateY znFlipbox--hAlignCenter znFlipbox--vAlignMiddle znFlipbox--speedNormal znFlipboxElm-pbOff" >

			<div class="znFlipbox znFlipbox-front"><div class="znFlipbox-content znFlipbox-contentFront"><div class="row zn_columns_container zn_content zn_col_container-flipbox" data-droplevel="1">
		<div class="eluid5ef366de      col-md-12 col-sm-12   znColumnElement"  id="eluid5ef366de" >


			<div class="znColumnElement-innerWrapper-eluid5ef366de znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="kl-iconbox eluidccce8e9a   kl-iconbox--type-img   kl-iconbox--align-center text-center kl-iconbox--theme-light element-scheme--light"  id="eluidccce8e9a">
	<div class="kl-iconbox__inner clearfix">


				<div class="kl-iconbox__icon-wrapper ">
			<img class="kl-iconbox__icon" src="/wp-content/uploads/2019/01/search-engine-optimaization.svg"  alt="" title="search-engine-optimaization">		</div><!-- /.kl-iconbox__icon-wrapper -->


		<div class="kl-iconbox__content-wrapper">




		</div><!-- /.kl-iconbox__content-wrapper -->

	</div>
</div>

<div class="kl-title-block clearfix tbk--text- tbk--center text-center tbk-symbol--  tbk-icon-pos--after-title eluidd36ff786 " ><h3 class="tbk__title" itemprop="headline" >SEO</h3><p class="tbk__subtitle" itemprop="alternativeHeadline" >Search Engine Optimization</p></div>				</div>
			</div>


		</div>
	</div></div><div class="znFlipbox-overlayFront"></div></div><div class="znFlipbox znFlipbox-back"><div class="znFlipbox-content znFlipbox-contentBack"><div class="row zn_columns_container zn_content zn_col_container-flipbox" data-droplevel="1">
		<div class="eluid7e5affe3      col-md-12 col-sm-12   znColumnElement"  id="eluid7e5affe3" >


			<div class="znColumnElement-innerWrapper-eluid7e5affe3 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-center znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="kl-iconbox eluid05f1ad29   kl-iconbox--type-icon   kl-iconbox--align-center text-center kl-iconbox--theme-light element-scheme--light"  id="eluid05f1ad29">
	<div class="kl-iconbox__inner clearfix">


				<div class="kl-iconbox__icon-wrapper ">
			<span class="kl-iconbox__icon kl-iconbox__icon--" data-zniconfam="icomoon" data-zn_icon=""></span>		</div><!-- /.kl-iconbox__icon-wrapper -->


		<div class="kl-iconbox__content-wrapper">


						<div class=" kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
				<p class="kl-iconbox__desc">We create SEO campaigns that drive organic search traffic through the creation of high-quality content </p>
			</div>


		</div><!-- /.kl-iconbox__content-wrapper -->

	</div>
</div>

				</div>
			</div>


		</div>
	</div></div><div class="znFlipbox-overlayBack"></div></div>
		</div>

						</div>
			</div>


		</div>

		<div class="eluidaa6d4b3b      col-md-3 col-sm-3   znColumnElement"  id="eluidaa6d4b3b" >


			<div class="znColumnElement-innerWrapper-eluidaa6d4b3b znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">

		<div class="znFlipboxElm eluidce1c9b03  znFlipbox--shadowsHover znFlipbox--rotateX znFlipbox--hAlignCenter znFlipbox--vAlignMiddle znFlipbox--speedNormal znFlipboxElm-pbOff" >

			<div class="znFlipbox znFlipbox-front"><div class="znFlipbox-content znFlipbox-contentFront"><div class="row zn_columns_container zn_content zn_col_container-flipbox" data-droplevel="1">
		<div class="eluida9332e3c      col-md-12 col-sm-12   znColumnElement"  id="eluida9332e3c" >


			<div class="znColumnElement-innerWrapper-eluida9332e3c znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="kl-iconbox eluidac6dcddf   kl-iconbox--type-img   kl-iconbox--align-center text-center kl-iconbox--theme-light element-scheme--light"  id="eluidac6dcddf">
	<div class="kl-iconbox__inner clearfix">


				<div class="kl-iconbox__icon-wrapper ">
			<img class="kl-iconbox__icon" src="/wp-content/uploads/2019/01/digital-marketing.svg"  alt="" title="digital-marketing">		</div><!-- /.kl-iconbox__icon-wrapper -->


		<div class="kl-iconbox__content-wrapper">




		</div><!-- /.kl-iconbox__content-wrapper -->

	</div>
</div>

<div class="kl-title-block clearfix tbk--text- tbk--center text-center tbk-symbol--  tbk-icon-pos--after-title eluid99f8643e " ><h3 class="tbk__title" itemprop="headline" >PPC</h3><h4 class="tbk__subtitle" itemprop="alternativeHeadline" >Pay Per Click Marketing</h4></div>				</div>
			</div>


		</div>
	</div></div><div class="znFlipbox-overlayFront"></div></div><div class="znFlipbox znFlipbox-back"><div class="znFlipbox-content znFlipbox-contentBack"><div class="row zn_columns_container zn_content zn_col_container-flipbox" data-droplevel="1">
		<div class="eluid70588ee8      col-md-12 col-sm-12   znColumnElement"  id="eluid70588ee8" >


			<div class="znColumnElement-innerWrapper-eluid70588ee8 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-center znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="kl-iconbox eluiddaac7fce   kl-iconbox--type-icon   kl-iconbox--align-center text-center kl-iconbox--theme-light element-scheme--light"  id="eluiddaac7fce">
	<div class="kl-iconbox__inner clearfix">


				<div class="kl-iconbox__icon-wrapper ">
			<span class="kl-iconbox__icon kl-iconbox__icon--" data-zniconfam="fontello" data-zn_icon=""></span>		</div><!-- /.kl-iconbox__icon-wrapper -->


		<div class="kl-iconbox__content-wrapper">


						<div class=" kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
				<p class="kl-iconbox__desc">Pay-Per-Click (PPC) is a highly cost-effective method of advertising on both search engines and websites</p>
			</div>


		</div><!-- /.kl-iconbox__content-wrapper -->

	</div>
</div>

				</div>
			</div>


		</div>
	</div></div><div class="znFlipbox-overlayBack"></div></div>
		</div>

						</div>
			</div>


		</div>

		<div class="eluid31c0dc0d      col-md-3 col-sm-3   znColumnElement"  id="eluid31c0dc0d" >


			<div class="znColumnElement-innerWrapper-eluid31c0dc0d znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">

		<div class="znFlipboxElm eluideeca264a  znFlipbox--shadowsHover znFlipbox--rotateY znFlipbox--hAlignCenter znFlipbox--vAlignMiddle znFlipbox--speedNormal znFlipboxElm-pbOff" >

			<div class="znFlipbox znFlipbox-front"><div class="znFlipbox-content znFlipbox-contentFront"><div class="row zn_columns_container zn_content zn_col_container-flipbox" data-droplevel="1">
		<div class="eluide8e7bff9      col-md-12 col-sm-12   znColumnElement"  id="eluide8e7bff9" >


			<div class="znColumnElement-innerWrapper-eluide8e7bff9 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="kl-iconbox eluid38e0623a   kl-iconbox--type-img   kl-iconbox--align-center text-center kl-iconbox--theme-light element-scheme--light"  id="eluid38e0623a">
	<div class="kl-iconbox__inner clearfix">


				<div class="kl-iconbox__icon-wrapper ">
			<img class="kl-iconbox__icon" src="/wp-content/uploads/2019/01/social-media.svg"  alt="" title="social-media">		</div><!-- /.kl-iconbox__icon-wrapper -->


		<div class="kl-iconbox__content-wrapper">




		</div><!-- /.kl-iconbox__content-wrapper -->

	</div>
</div>

<div class="kl-title-block clearfix tbk--text- tbk--center text-center tbk-symbol--  tbk-icon-pos--after-title eluidd68b059a " ><h3 class="tbk__title" itemprop="headline" >SMO</h3><h4 class="tbk__subtitle" itemprop="alternativeHeadline" >Social Media Optimization</h4></div>				</div>
			</div>


		</div>
	</div></div><div class="znFlipbox-overlayFront"></div></div><div class="znFlipbox znFlipbox-back"><div class="znFlipbox-content znFlipbox-contentBack"><div class="row zn_columns_container zn_content zn_col_container-flipbox" data-droplevel="1">
		<div class="eluidc174c543      col-md-12 col-sm-12   znColumnElement"  id="eluidc174c543" >


			<div class="znColumnElement-innerWrapper-eluidc174c543 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-center znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="kl-iconbox eluidb95324ca   kl-iconbox--type-icon   kl-iconbox--align-center text-center kl-iconbox--theme-light element-scheme--light"  id="eluidb95324ca">
	<div class="kl-iconbox__inner clearfix">


				<div class="kl-iconbox__icon-wrapper ">
			<span class="kl-iconbox__icon kl-iconbox__icon--" data-zniconfam="icomoon" data-zn_icon=""></span>		</div><!-- /.kl-iconbox__icon-wrapper -->


		<div class="kl-iconbox__content-wrapper">


						<div class=" kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
				<p class="kl-iconbox__desc">We utilize social media channels to drive more website traffic to your website and increase sales</p>
			</div>


		</div><!-- /.kl-iconbox__content-wrapper -->

	</div>
</div>

				</div>
			</div>


		</div>
	</div></div><div class="znFlipbox-overlayBack"></div></div>
		</div>

						</div>
			</div>


		</div>

		<div class="eluid347a508d      col-md-3 col-sm-3   znColumnElement"  id="eluid347a508d" >


			<div class="znColumnElement-innerWrapper-eluid347a508d znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">

		<div class="znFlipboxElm eluid325d5ae0  znFlipbox--shadowsHover znFlipbox--rotateY znFlipbox--hAlignCenter znFlipbox--vAlignMiddle znFlipbox--speedNormal znFlipboxElm-pbOff" >

			<div class="znFlipbox znFlipbox-front"><div class="znFlipbox-content znFlipbox-contentFront"><div class="row zn_columns_container zn_content zn_col_container-flipbox" data-droplevel="1">
		<div class="eluid97cab00a      col-md-12 col-sm-12   znColumnElement"  id="eluid97cab00a" >


			<div class="znColumnElement-innerWrapper-eluid97cab00a znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="kl-iconbox eluid8ba28b46   kl-iconbox--type-img   kl-iconbox--align-center text-center kl-iconbox--theme-light element-scheme--light"  id="eluid8ba28b46">
	<div class="kl-iconbox__inner clearfix">


				<div class="kl-iconbox__icon-wrapper ">
			<img class="kl-iconbox__icon" src="/wp-content/uploads/2019/01/ecommerce.svg"  alt="" title="ecommerce">		</div><!-- /.kl-iconbox__icon-wrapper -->


		<div class="kl-iconbox__content-wrapper">




		</div><!-- /.kl-iconbox__content-wrapper -->

	</div>
</div>

<div class="kl-title-block clearfix tbk--text- tbk--center text-center tbk-symbol--  tbk-icon-pos--after-title eluid71f04715 " ><h3 class="tbk__title" itemprop="headline" >eCOMMERCE</h3><h4 class="tbk__subtitle" itemprop="alternativeHeadline" >E-commerce websites</h4></div>				</div>
			</div>


		</div>
	</div></div><div class="znFlipbox-overlayFront"></div></div><div class="znFlipbox znFlipbox-back"><div class="znFlipbox-content znFlipbox-contentBack"><div class="row zn_columns_container zn_content zn_col_container-flipbox" data-droplevel="1">
		<div class="eluid6e40fd1f      col-md-12 col-sm-12   znColumnElement"  id="eluid6e40fd1f" >


			<div class="znColumnElement-innerWrapper-eluid6e40fd1f znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-center znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="kl-iconbox eluid9189fbc2   kl-iconbox--type-icon   kl-iconbox--align-center text-center kl-iconbox--theme-light element-scheme--light"  id="eluid9189fbc2">
	<div class="kl-iconbox__inner clearfix">


				<div class="kl-iconbox__icon-wrapper ">
			<span class="kl-iconbox__icon kl-iconbox__icon--" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span>		</div><!-- /.kl-iconbox__icon-wrapper -->


		<div class="kl-iconbox__content-wrapper">


						<div class=" kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
				<p class="kl-iconbox__desc">E-Commerce websites are online portals that facilitate online transactions of products and services.</p>
			</div>


		</div><!-- /.kl-iconbox__content-wrapper -->

	</div>
</div>

				</div>
			</div>


		</div>
	</div></div><div class="znFlipbox-overlayBack"></div></div>
		</div>

						</div>
			</div>


		</div>

		<div class="eluid4f6e7077      col-md-3 col-sm-3   znColumnElement"  id="eluid4f6e7077" >


			<div class="znColumnElement-innerWrapper-eluid4f6e7077 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">

		<div class="znFlipboxElm eluid1a6ca170  znFlipbox--shadowsHover znFlipbox--rotateX znFlipbox--hAlignCenter znFlipbox--vAlignMiddle znFlipbox--speedNormal znFlipboxElm-pbOff" >

			<div class="znFlipbox znFlipbox-front"><div class="znFlipbox-content znFlipbox-contentFront"><div class="row zn_columns_container zn_content zn_col_container-flipbox" data-droplevel="1">
		<div class="eluidb0d99edf      col-md-12 col-sm-12   znColumnElement"  id="eluidb0d99edf" >


			<div class="znColumnElement-innerWrapper-eluidb0d99edf znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="kl-iconbox eluid4d6f9271   kl-iconbox--type-img   kl-iconbox--align-center text-center kl-iconbox--theme-light element-scheme--light"  id="eluid4d6f9271">
	<div class="kl-iconbox__inner clearfix">


				<div class="kl-iconbox__icon-wrapper ">
			<img class="kl-iconbox__icon" src="/wp-content/uploads/2019/01/content-devlopment.svg"  alt="" title="content-devlopment">		</div><!-- /.kl-iconbox__icon-wrapper -->


		<div class="kl-iconbox__content-wrapper">




		</div><!-- /.kl-iconbox__content-wrapper -->

	</div>
</div>

<div class="kl-title-block clearfix tbk--text- tbk--center text-center tbk-symbol--  tbk-icon-pos--after-title eluidbcf52654 " ><h3 class="tbk__title" itemprop="headline" >CONTENT</h3><h4 class="tbk__subtitle" itemprop="alternativeHeadline" >Content Development </h4></div>				</div>
			</div>


		</div>
	</div></div><div class="znFlipbox-overlayFront"></div></div><div class="znFlipbox znFlipbox-back"><div class="znFlipbox-content znFlipbox-contentBack"><div class="row zn_columns_container zn_content zn_col_container-flipbox" data-droplevel="1">
		<div class="eluid14a911c1      col-md-12 col-sm-12   znColumnElement"  id="eluid14a911c1" >


			<div class="znColumnElement-innerWrapper-eluid14a911c1 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-center znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="kl-iconbox eluidcd298793   kl-iconbox--type-icon   kl-iconbox--align-center text-center kl-iconbox--theme-light element-scheme--light"  id="eluidcd298793">
	<div class="kl-iconbox__inner clearfix">


				<div class="kl-iconbox__icon-wrapper ">
			<span class="kl-iconbox__icon kl-iconbox__icon--" data-zniconfam="kl-social-icons" data-zn_icon=""></span>		</div><!-- /.kl-iconbox__icon-wrapper -->


		<div class="kl-iconbox__content-wrapper">


						<div class=" kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
				<p class="kl-iconbox__desc">We provide secure and scalable service for all your domains </p>
			</div>


		</div><!-- /.kl-iconbox__content-wrapper -->

	</div>
</div>

				</div>
			</div>


		</div>
	</div></div><div class="znFlipbox-overlayBack"></div></div>
		</div>

						</div>
			</div>


		</div>

		<div class="eluid161ecbca      col-md-3 col-sm-3   znColumnElement"  id="eluid161ecbca" >


			<div class="znColumnElement-innerWrapper-eluid161ecbca znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">

		<div class="znFlipboxElm eluidf02f6cb3  znFlipbox--shadowsHover znFlipbox--rotateY znFlipbox--hAlignCenter znFlipbox--vAlignMiddle znFlipbox--speedNormal znFlipboxElm-pbOff" >

			<div class="znFlipbox znFlipbox-front"><div class="znFlipbox-content znFlipbox-contentFront"><div class="row zn_columns_container zn_content zn_col_container-flipbox" data-droplevel="1">
		<div class="eluid89a6d174      col-md-12 col-sm-12   znColumnElement"  id="eluid89a6d174" >


			<div class="znColumnElement-innerWrapper-eluid89a6d174 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="kl-iconbox eluid874efa5e   kl-iconbox--type-img   kl-iconbox--align-center text-center kl-iconbox--theme-light element-scheme--light"  id="eluid874efa5e">
	<div class="kl-iconbox__inner clearfix">


				<div class="kl-iconbox__icon-wrapper ">
			<img class="kl-iconbox__icon" src="/wp-content/uploads/2019/01/cloud-hosting.svg"  alt="" title="cloud-hosting">		</div><!-- /.kl-iconbox__icon-wrapper -->


		<div class="kl-iconbox__content-wrapper">




		</div><!-- /.kl-iconbox__content-wrapper -->

	</div>
</div>

<div class="kl-title-block clearfix tbk--text- tbk--center text-center tbk-symbol--  tbk-icon-pos--after-title eluidd535fdf5 " ><h3 class="tbk__title" itemprop="headline" >CLOUD</h3><h4 class="tbk__subtitle" itemprop="alternativeHeadline" >Cloud Hosting</h4></div>				</div>
			</div>


		</div>
	</div></div><div class="znFlipbox-overlayFront"></div></div><div class="znFlipbox znFlipbox-back"><div class="znFlipbox-content znFlipbox-contentBack"><div class="row zn_columns_container zn_content zn_col_container-flipbox" data-droplevel="1">
		<div class="eluid699d88a1      col-md-12 col-sm-12   znColumnElement"  id="eluid699d88a1" >


			<div class="znColumnElement-innerWrapper-eluid699d88a1 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-center znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="kl-iconbox eluide5a5c2ff   kl-iconbox--type-icon   kl-iconbox--align-center text-center kl-iconbox--theme-light element-scheme--light"  id="eluide5a5c2ff">
	<div class="kl-iconbox__inner clearfix">


				<div class="kl-iconbox__icon-wrapper ">
			<span class="kl-iconbox__icon kl-iconbox__icon--" data-zniconfam="icomoon" data-zn_icon=""></span>		</div><!-- /.kl-iconbox__icon-wrapper -->


		<div class="kl-iconbox__content-wrapper">


						<div class=" kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
				<p class="kl-iconbox__desc">Our content development strategies leverage traffics for your business and generates on-time results.</p>
			</div>


		</div><!-- /.kl-iconbox__content-wrapper -->

	</div>
</div>

				</div>
			</div>


		</div>
	</div></div><div class="znFlipbox-overlayBack"></div></div>
		</div>

						</div>
			</div>


		</div>

		<div class="eluid01a662ff      col-md-3 col-sm-3   znColumnElement"  id="eluid01a662ff" >


			<div class="znColumnElement-innerWrapper-eluid01a662ff znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">

		<div class="znFlipboxElm eluidc2b03efb  znFlipbox--shadowsHover znFlipbox--rotateX znFlipbox--hAlignCenter znFlipbox--vAlignMiddle znFlipbox--speedNormal znFlipboxElm-pbOff" >

			<div class="znFlipbox znFlipbox-front"><div class="znFlipbox-content znFlipbox-contentFront"><div class="row zn_columns_container zn_content zn_col_container-flipbox" data-droplevel="1">
		<div class="eluid4747506b      col-md-12 col-sm-12   znColumnElement"  id="eluid4747506b" >


			<div class="znColumnElement-innerWrapper-eluid4747506b znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="kl-iconbox eluiddb963fb2   kl-iconbox--type-img   kl-iconbox--align-center text-center kl-iconbox--theme-light element-scheme--light"  id="eluiddb963fb2">
	<div class="kl-iconbox__inner clearfix">


				<div class="kl-iconbox__icon-wrapper ">
			<img class="kl-iconbox__icon" src="/wp-content/uploads/2019/01/mobile-apps-software-development.svg"  alt="" title="mobile apps software development">		</div><!-- /.kl-iconbox__icon-wrapper -->


		<div class="kl-iconbox__content-wrapper">




		</div><!-- /.kl-iconbox__content-wrapper -->

	</div>
</div>

<div class="kl-title-block clearfix tbk--text- tbk--center text-center tbk-symbol--  tbk-icon-pos--after-title eluid8cfd4842 " ><h3 class="tbk__title" itemprop="headline" >SYSTEM</h3><h4 class="tbk__subtitle" itemprop="alternativeHeadline" >Mobile & Software Development</h4></div>				</div>
			</div>


		</div>
	</div></div><div class="znFlipbox-overlayFront"></div></div><div class="znFlipbox znFlipbox-back"><div class="znFlipbox-content znFlipbox-contentBack"><div class="row zn_columns_container zn_content zn_col_container-flipbox" data-droplevel="1">
		<div class="eluid9dcd2b58      col-md-12 col-sm-12   znColumnElement"  id="eluid9dcd2b58" >


			<div class="znColumnElement-innerWrapper-eluid9dcd2b58 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-center znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="kl-iconbox eluidc84e9490   kl-iconbox--type-icon   kl-iconbox--align-center text-center kl-iconbox--theme-light element-scheme--light"  id="eluidc84e9490">
	<div class="kl-iconbox__inner clearfix">


				<div class="kl-iconbox__icon-wrapper ">
			<span class="kl-iconbox__icon kl-iconbox__icon--" data-zniconfam="icomoon" data-zn_icon=""></span>		</div><!-- /.kl-iconbox__icon-wrapper -->


		<div class="kl-iconbox__content-wrapper">


						<div class=" kl-iconbox__el-wrapper kl-iconbox__desc-wrapper">
				<p class="kl-iconbox__desc">We design custom and responsive mobile & software applications for iOS and Android devices.</p>
			</div>


		</div><!-- /.kl-iconbox__content-wrapper -->

	</div>
</div>

				</div>
			</div>


		</div>
	</div></div><div class="znFlipbox-overlayBack"></div></div>
		</div>

						</div>
			</div>


		</div>

				</div>
			</div>

					</section>


		<div class="eluide705e690 " >		<section class="zn_section eluida8972f38     section-sidemargins    zn_section--relative section--no " id="eluida8972f38"  >

			<div class="zn-bgSource "  ><div class="zn-bgSource-image" style="background-image:url(/wp-content/uploads/2018/06/bg-gr-img.jpg);background-repeat:no-repeat;background-position:center top;background-size:auto;background-attachment:scroll"></div></div>
			<div class="zn_section_size container zn-section-height--auto zn-section-content_algn--top ">

				<div class="row ">

		<div class="eluid8f99f853      col-md-12 col-sm-12   znColumnElement"  id="eluid8f99f853" >


			<div class="znColumnElement-innerWrapper-eluid8f99f853 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="kl-title-block clearfix tbk--text- tbk--center text-center tbk-symbol-- tbk--colored tbk-icon-pos--after-title eluidde5fd3c4 " ><h1 class="tbk__title" itemprop="headline" >Recent Web Projects</h1></div>				</div>
			</div>


		</div>

		<div class="eluidb2c9a1b2      col-md-12 col-sm-12   znColumnElement"  id="eluidb2c9a1b2" >


			<div class="znColumnElement-innerWrapper-eluidb2c9a1b2 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="znSmartCarousel eluide1c8cf4a   znSmartCarouselMode--view element-scheme--light" ><div class="znSmartCarousel-holder js-slick " data-slick='{"infinite":true,"slidesToShow":1,"slidesToScroll":1,"autoplay":true,"autoplaySpeed":6000,"easing":"linear","speed":500,"swipe":true,"touchMove":true,"adaptiveHeight":false,"responsive":{"breakpoint":1024,"settings":{"slidesToScroll":1}},"dots":false,"arrows":true,"appendArrows":".eluide1c8cf4a .znSmartCarousel-nav","prevArrow":".eluide1c8cf4a .znSmartCarousel-prev","nextArrow":".eluide1c8cf4a .znSmartCarousel-next"}'><div class="znSmartCarousel-item znSmartCarousel-item--1 " id="eluide1c8cf4a_0"><div class="row znSmartCarousel-container ">
		<div class="eluidf41c4819      col-md-4 col-sm-4   znColumnElement"  id="eluidf41c4819" >


			<div class="znColumnElement-innerWrapper-eluidf41c4819 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-center znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="image-boxes imgbox-simple eluiddb47d79c " ><div class="image-boxes-holder imgboxes-wrapper u-mb-0  "><div class="image-boxes-img-wrapper img-align-center"><img class="image-boxes-img img-responsive " src="/wp-content/uploads/2018/06/SLIIT.png"   alt="SLIIT"  title="SLIIT" /></div></div></div><div class="kl-title-block clearfix tbk--text- tbk--center text-center tbk-symbol--  tbk-icon-pos--after-title eluid8d20cadb recent-project-title-bg" ><h3 class="tbk__title" itemprop="headline" >SLIIT</h3><h4 class="tbk__subtitle" itemprop="alternativeHeadline" >Malabe, Sri Lanka</h4></div><div class="zn_text_box eluidb593afb2  zn_text_box-light element-scheme--light" ><p style="text-align: center;">web development / seo / analytics / content</p>
</div><div class="zn-buttonWrapper zn-buttonWrapper-eluidfd3f0bec text-center"><a href="//goo.gl/XJmMQY" id="eluidfd3f0bec" class="eluidfd3f0bec  zn-button btn btn-fullcolor btn-custom-color btn--square btn-icon--before" title="View Project" target="_blank" itemprop="url" ><span class="zn-buttonText">View Project</span></a></div>				</div>
			</div>


		</div>

		<div class="eluid8a28413e res-slider-recent     col-md-8 col-sm-8   znColumnElement"  id="eluid8a28413e" >


			<div class="znColumnElement-innerWrapper-eluid8a28413e znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-center znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="image-boxes imgbox-simple eluida90c593d " ><div class="image-boxes-holder imgboxes-wrapper u-mb-0  "><div class="image-boxes-img-wrapper img-align-center"><img class="image-boxes-img img-responsive " src="/wp-content/uploads/2018/06/Responsive-Design-sliit.png"   alt="Responsive Design-sliit"  title="Responsive Design-sliit" /></div></div></div>				</div>
			</div>


		</div>
	</div></div><div class="znSmartCarousel-item znSmartCarousel-item--2 " id="eluide1c8cf4a_1"><div class="row znSmartCarousel-container ">
		<div class="eluid80a3547e      col-md-4 col-sm-4   znColumnElement"  id="eluid80a3547e" >


			<div class="znColumnElement-innerWrapper-eluid80a3547e znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-center znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="image-boxes imgbox-simple eluidcac564c1 " ><div class="image-boxes-holder imgboxes-wrapper u-mb-0  "><div class="image-boxes-img-wrapper img-align-center"><img class="image-boxes-img img-responsive " src="/wp-content/uploads/2018/06/cameralk.png"   alt="cameralk"  title="cameralk" /></div></div></div><div class="kl-title-block clearfix tbk--text- tbk--center text-center tbk-symbol--  tbk-icon-pos--after-title eluidbf1ed687 recent-project-title-bg" ><h3 class="tbk__title" itemprop="headline" >CameraLK</h3><h4 class="tbk__subtitle" itemprop="alternativeHeadline" >263, Highlevel Road, Colombo 05 Sri Lanka</h4></div><div class="zn_text_box eluidc737641c  zn_text_box-light element-scheme--light" ><p style="text-align: center;">web development / seo / analytics / content</p>
</div><div class="zn-buttonWrapper zn-buttonWrapper-eluidca0722aa text-center"><a href="//goo.gl/DWJ1eW" id="eluidca0722aa" class="eluidca0722aa  zn-button btn btn-fullcolor btn-custom-color btn--square btn-icon--before" title="View Project" target="_blank" itemprop="url" ><span class="zn-buttonText">View Project</span></a></div>				</div>
			</div>


		</div>

		<div class="eluid173eb9c6 res-slider-recent     col-md-8 col-sm-8   znColumnElement"  id="eluid173eb9c6" >


			<div class="znColumnElement-innerWrapper-eluid173eb9c6 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-center znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="image-boxes imgbox-simple eluid44709c47 " ><div class="image-boxes-holder imgboxes-wrapper u-mb-0  "><div class="image-boxes-img-wrapper img-align-center"><img class="image-boxes-img img-responsive " src="/wp-content/uploads/2018/06/Responsive-Design-cameralk.png"   alt="Responsive Design-cameralk"  title="Responsive Design-cameralk" /></div></div></div>				</div>
			</div>


		</div>
	</div></div><div class="znSmartCarousel-item znSmartCarousel-item--3 " id="eluide1c8cf4a_2"><div class="row znSmartCarousel-container ">
		<div class="eluid011c4c82      col-md-4 col-sm-4   znColumnElement"  id="eluid011c4c82" >


			<div class="znColumnElement-innerWrapper-eluid011c4c82 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-center znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="image-boxes imgbox-simple eluid688af283 " ><div class="image-boxes-holder imgboxes-wrapper u-mb-0  "><div class="image-boxes-img-wrapper img-align-center"><img class="image-boxes-img img-responsive " src="/wp-content/uploads/2018/06/etisalat.png"   alt="etisalat"  title="etisalat" /></div></div></div><div class="kl-title-block clearfix tbk--text- tbk--center text-center tbk-symbol--  tbk-icon-pos--after-title eluid41784d51 recent-project-title-bg" ><h3 class="tbk__title" itemprop="headline" >Etisalat Lanka</h3><h4 class="tbk__subtitle" itemprop="alternativeHeadline" >No.109, Galle Road, Colombo 03</h4></div><div class="zn_text_box eluid8ea9213d  zn_text_box-light element-scheme--light" ><p style="text-align: center;">web development / seo / analytics / content</p>
</div><div class="zn-buttonWrapper zn-buttonWrapper-eluid8a3fb4ba text-center"><a href="//goo.gl/vGvuYJ" id="eluid8a3fb4ba" class="eluid8a3fb4ba  zn-button btn btn-fullcolor btn-custom-color btn--square btn-icon--before" title="View Project" target="_blank" itemprop="url" ><span class="zn-buttonText">View Project</span></a></div>				</div>
			</div>


		</div>

		<div class="eluid082e7d35 res-slider-recent     col-md-8 col-sm-8   znColumnElement"  id="eluid082e7d35" >


			<div class="znColumnElement-innerWrapper-eluid082e7d35 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-center znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="image-boxes imgbox-simple eluid41e658ca " ><div class="image-boxes-holder imgboxes-wrapper u-mb-0  "><div class="image-boxes-img-wrapper img-align-center"><img class="image-boxes-img img-responsive " src="/wp-content/uploads/2018/06/Responsive-Design-Etisalat.png"   alt="Responsive Design-Etisalat"  title="Responsive Design-Etisalat" /></div></div></div>				</div>
			</div>


		</div>
	</div></div><div class="znSmartCarousel-item znSmartCarousel-item--4 " id="eluide1c8cf4a_3"><div class="row znSmartCarousel-container ">
		<div class="eluideffb0617      col-md-4 col-sm-4   znColumnElement"  id="eluideffb0617" >


			<div class="znColumnElement-innerWrapper-eluideffb0617 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-center znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="image-boxes imgbox-simple eluid00a898b6 " ><div class="image-boxes-holder imgboxes-wrapper u-mb-0  "><div class="image-boxes-img-wrapper img-align-center"><img class="image-boxes-img img-responsive " src="/wp-content/uploads/2018/06/ooredoo.png"   alt="ooredoo"  title="ooredoo" /></div></div></div><div class="kl-title-block clearfix tbk--text- tbk--center text-center tbk-symbol--  tbk-icon-pos--after-title eluid0619f3a0 recent-project-title-bg" ><h3 class="tbk__title" itemprop="headline" >Oredoo Maldives </h3><h4 class="tbk__subtitle" itemprop="alternativeHeadline" >Maldives </h4></div><div class="zn_text_box eluid361d8b97  zn_text_box-light element-scheme--light" ><p style="text-align: center;">web development / seo / analytics / content</p>
</div><div class="zn-buttonWrapper zn-buttonWrapper-eluid28e6dada text-center"><a href="//goo.gl/RwDPEF" id="eluid28e6dada" class="eluid28e6dada  zn-button btn btn-fullcolor btn-custom-color btn--square btn-icon--before" title="View Project" target="_blank" itemprop="url" ><span class="zn-buttonText">View Project</span></a></div>				</div>
			</div>


		</div>

		<div class="eluid1588222f res-slider-recent     col-md-8 col-sm-8   znColumnElement"  id="eluid1588222f" >


			<div class="znColumnElement-innerWrapper-eluid1588222f znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-center znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="image-boxes imgbox-simple eluid5c9f1c55 " ><div class="image-boxes-holder imgboxes-wrapper u-mb-0  "><div class="image-boxes-img-wrapper img-align-center"><img class="image-boxes-img img-responsive " src="/wp-content/uploads/2018/06/Responsive-Design-ooredoo.png"   alt="Responsive Design-ooredoo"  title="Responsive Design-ooredoo" /></div></div></div>				</div>
			</div>


		</div>
	</div></div></div><div class="znSmartCarousel-nav znSmartCarousel-navPosition--bottom-left znSmartCarousel-navStyle--s1"><span class="znSmartCarousel-arr znSmartCarousel-prev "><svg viewBox="0 0 256 256"><polyline fill="none" stroke="black" stroke-width="16" stroke-linejoin="round" stroke-linecap="round" points="184,16 72,128 184,240"/></svg></span><span class="znSmartCarousel-arr znSmartCarousel-next"><svg viewBox="0 0 256 256"><polyline fill="none" stroke="black" stroke-width="16" stroke-linejoin="round" stroke-linecap="round" points="72,16 184,128 72,240"/></svg></span></div><div class="clearfix"></div>
			<div class="clearfix"></div>
</div>				</div>
			</div>


		</div>

		<div class="eluid013cb662      col-md-12 col-sm-12   znColumnElement"  id="eluid013cb662" >


			<div class="znColumnElement-innerWrapper-eluid013cb662 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="zn-buttonWrapper zn-buttonWrapper-eluidc43c028d text-center"><a href="/our-works/" id="eluidc43c028d" class="eluidc43c028d  zn-button btn btn-fullcolor btn-custom-color btn--square btn-icon--before" title="View All Project" target="_blank" itemprop="url" ><span class="zn-buttonText">View All Project</span></a></div>				</div>
			</div>


		</div>

				</div>
			</div>

					</section>


		</div>		<section class="zn_section eluid3e0f60c9     section-sidemargins    zn_section--masked zn_section--relative section--no " id="eluid3e0f60c9"  >

			<div class="zn-bgSource "  ><div class="zn-bgSource-image" style="background-image:url(/wp-content/uploads/2018/06/blackmamba.png);background-repeat:repeat;background-position:center center;background-size:auto;background-attachment:scroll"></div></div>
			<div class="zn_section_size full_width zn-section-height--auto zn-section-content_algn--middle ">

				<div class="row gutter-0">

		<div class="eluid3d7d5a4f      col-md-6 col-sm-6   znColumnElement"  id="eluid3d7d5a4f" >


			<div class="znColumnElement-innerWrapper-eluid3d7d5a4f znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-center znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="kl-title-block clearfix tbk--text- tbk--center text-center tbk-symbol--line_border tbk--colored tbk-icon-pos--after-title eluid5211d881 " ><h1 class="tbk__title" itemprop="headline" >Digital Marketing Solutions</h1><span class="tbk__symbol "><span></span></span><div class="tbk__text"><p style="text-align: center;"><span style="color: #ffffff;">We craft result-driven digital marketing strategies to increase brand visibility, traffic and sales from your ideal buyers. All our campaigns are completely bespoke, flexible and scalable to meet the diverse objectives of corporate and SME clients. And most importantly, you’ll always know exactly what we’re doing, how we’re doing it and get frequent reports to chart the success of the campaigns.</span></p>
</div></div>
<div class="grid-ibx grid-ibx--cols-3 grid-ibx--md-cols-3 grid-ibx--sm-cols-2 grid-ibx--xs-cols-1 grid-ibx--hover-scale eluida1a18141  grid-ibx--theme-light element-scheme--light grid-ibx__flt-  " id="eluida1a18141">
	<div class="grid-ibx__inner">
		<div class="grid-ibx__row clearfix">
					<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-0">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/seo.png"  width="150" height="150" alt="Web lankan- SEO-digital marketing-web designing" title="Web lankan- SEO-digital marketing-web designing" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-1">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/ppc.png"  width="150" height="150" alt="Web lankan- PPC-digital marketing-web designing" title="Web lankan- PPC-digital marketing-web designing" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-2">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/07/smo.png"  width="150" height="150" alt="smo- Web Lankan" title="smo- Web Lankan" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->

	</div><!-- /.grid-ibx__row -->
	</div>
</div><!-- /.grid-ibx -->

<div class="zn-buttonWrapper zn-buttonWrapper-eluid96340fc1 text-center"><a href="/digital-marketing/" id="eluid96340fc1" class="eluid96340fc1  zn-button btn btn-fullcolor btn-custom-color btn--square btn-icon--before" title="More Info" target="_self" itemprop="url" ><span class="zn-buttonText">More Info</span></a></div>				</div>
			</div>


		</div>

		<div class="eluidd85469aa      col-md-6 col-sm-6   znColumnElement"  id="eluidd85469aa" >


			<div class="znColumnElement-innerWrapper-eluidd85469aa znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-center znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="kl-title-block clearfix tbk--text- tbk--left text-left tbk-symbol--line_border tbk--colored tbk-icon-pos--after-title eluid8c5092fa " ><h1 class="tbk__title" itemprop="headline" >E-Commerce Solutions</h1><span class="tbk__symbol "><span></span></span><div class="tbk__text"><p style="text-align: left;"><span style="color: #ffffff;">We craft result-driven digital marketing strategies to increase brand visibility, traffic and sales from your ideal buyers. All our campaigns are completely bespoke, flexible and scalable to meet the diverse objectives of corporate and SME clients. And most importantly, you’ll always know exactly what we’re doing, how we’re doing it and get frequent reports to chart the success of the campaigns.</span></p>
<p style="text-align: left;"><span style="color: #ffffff;">Having a robust eCommerce website is the foundation of your online retail business, and we understand the importance of utilizing the latest technology to increase your profitability.</span></p>
</div></div>
<div class="grid-ibx grid-ibx--cols-5 grid-ibx--md-cols-3 grid-ibx--sm-cols-2 grid-ibx--xs-cols-1 grid-ibx--hover-scale eluid2199115c ecommerce-icon grid-ibx--theme-light element-scheme--light grid-ibx__flt-  " id="eluid2199115c">
	<div class="grid-ibx__inner">
		<div class="grid-ibx__row clearfix">
					<div class="grid-ibx__item  grid-ibx__item--type-img text-left grid-ibx__item-0">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2019/01/weblankan-e-commerce-icons-1.png"  width="250" height="250" alt="" title="weblankan-e-commerce-icons (1)" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-left grid-ibx__item-1">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2019/01/weblankan-e-commerce-icons-2.png"  width="250" height="250" alt="" title="weblankan-e-commerce-icons (2)" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-left grid-ibx__item-2">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2019/01/weblankan-e-commerce-icons-3.png"  width="250" height="250" alt="" title="weblankan-e-commerce-icons (3)" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-left grid-ibx__item-3">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2019/01/weblankan-e-commerce-icons-4.png"  width="250" height="250" alt="" title="weblankan-e-commerce-icons (4)" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-left grid-ibx__item-4">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2019/01/weblankan-e-commerce-icons-5.png"  width="250" height="250" alt="" title="weblankan-e-commerce-icons (5)" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->

	</div><!-- /.grid-ibx__row -->
	</div>
</div><!-- /.grid-ibx -->

<div class="zn-buttonWrapper zn-buttonWrapper-eluid7a704646 text-left"><a href="/ecommerce-solutions/" id="eluid7a704646" class="eluid7a704646  zn-button btn btn-fullcolor btn-custom-color btn--square btn-icon--before" title="More Info" target="_self" itemprop="url" ><span class="zn-buttonText">More Info</span></a></div>				</div>
			</div>


		</div>

				</div>
			</div>

			<div class="kl-mask kl-topmask kl-mask--shadow_simple kl-mask--light"></div><div class="kl-mask kl-bottommask kl-mask--shadow_simple kl-mask--light"></div>		</section>


				<section class="zn_section eluid9a546cc9     section-sidemargins    section--no " id="eluid9a546cc9"  >


			<div class="zn_section_size container zn-section-height--auto zn-section-content_algn--top ">

				<div class="row ">

		<div class="eluide37383da      col-md-12 col-sm-12   znColumnElement"  id="eluide37383da" >


			<div class="znColumnElement-innerWrapper-eluide37383da znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="kl-title-block clearfix tbk--text- tbk--center text-center tbk-symbol--line_border tbk--colored tbk-icon-pos--after-title eluid4b2ca72c " ><h1 class="tbk__title" itemprop="headline" >Our Exclusive Clients</h1><span class="tbk__symbol "><span></span></span><h4 class="tbk__subtitle" itemprop="alternativeHeadline" >A handful of the special clients we have served so far...</h4></div>
<div class="grid-ibx grid-ibx--cols-5 grid-ibx--md-cols-5 grid-ibx--sm-cols-2 grid-ibx--xs-cols-1 grid-ibx--style-lined-center grid-ibx--hover-bg eluid9d0de1df  grid-ibx--theme-light element-scheme--light grid-ibx__flt-  " id="eluid9d0de1df">
	<div class="grid-ibx__inner">
		<div class="grid-ibx__row clearfix">
					<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-0">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/sliit-1.png"  width="136" height="76" alt="weblankan-client- SLIIT" title="weblankan-client- SLIIT" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-1">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/web-lankan-clients-debug-auto-ecclusive-.png"  width="136" height="76" alt="web-lankan-clients-debug-auto-ecclusive-" title="web-lankan-clients-debug-auto-ecclusive-" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-2">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/web-lankan-clients-Dp-Jayasinghe-.png"  width="136" height="76" alt="web-lankan-clients-Dp-Jayasinghe-" title="web-lankan-clients-Dp-Jayasinghe-" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-3">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/Web-lankan-clients-dsi-group-scom.jpg"  width="136" height="76" alt="Web-lankan-clients-DSI-group" title="Web-lankan-clients-DSI-group" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-4">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/weblankan-client-lhpiyasena.lk_.png"  width="136" height="76" alt="weblankan-client-lhpiyasena.lk_" title="weblankan-client-lhpiyasena.lk_" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-5">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/weblankan-client-blackpool.png"  width="136" height="76" alt="weblankan-client-blackpool" title="weblankan-client-blackpool" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-6">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/weblankan-client-luckylanka.png"  width="136" height="76" alt="weblankan-client-luckylanka" title="weblankan-client-luckylanka" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-7">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/web-lankan-clients-foster-reed-.png"  width="136" height="76" alt="web-lankan-clients-foster-reed-" title="web-lankan-clients-foster-reed-" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-8">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/web-lankan-clients-jat-holdings.jpg"  width="136" height="76" alt="web-lankan-clients-jat-holdings" title="web-lankan-clients-jat-holdings" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-9">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/web-lankan-clients-str-car-care.jpg"  width="136" height="76" alt="web-lankan-clients-str-car-care" title="web-lankan-clients-str-car-care" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-10">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/web-lankan-clients-Ibrahim-Jafferjee.jpg"  width="136" height="76" alt="web-lankan-clients-Ibrahim-Jafferjee" title="web-lankan-clients-Ibrahim-Jafferjee" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-11">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/web-lankan-clients-kapruka.jpg"  width="136" height="76" alt="web-lankan-clients-kapruka" title="web-lankan-clients-kapruka" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-12">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/web-lankan-clients-subasingha.jpg"  width="136" height="76" alt="web-lankan-clients-subasingha" title="web-lankan-clients-subasingha" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-13">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/web-lankan-clients-pearl-grand-city-hotel.jpg"  width="136" height="76" alt="web-lankan-clients-pearl-grand-city-hotel" title="web-lankan-clients-pearl-grand-city-hotel" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-14">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/aminra.jpg"  width="136" height="76" alt="Web-lankan-clients-Aminra" title="Web-lankan-clients-Aminra" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-15">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/09/web-lankan-clients-super-kids-sri-lanka-a.jpg"  width="136" height="76" alt="" title="web-lankan-clients-super-kids-sri-lanka-a" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-16">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/weblankan-client-salaka.png"  width="136" height="76" alt="weblankan-client-salaka" title="weblankan-client-salaka" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-17">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/web-lankan-clients-Sk.jpg"  width="136" height="76" alt="web-lankan-clients-S & k" title="web-lankan-clients-S & k" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-18">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/web-lankan-clients-kuma-iddamallena.jpg"  width="136" height="76" alt="web-lankan-clients-kuma-iddamallena" title="web-lankan-clients-kuma-iddamallena" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-19">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/web-lankan-clients-suvika.jpg"  width="136" height="76" alt="web-lankan-clients-suvika" title="web-lankan-clients-suvika" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-20">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/walkers-cml.png"  width="136" height="76" alt="weblankan-client-walkers CML" title="weblankan-client-walkers CML" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-21">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/09/web-lankan-clients-super-kids-sri-lanka-b.jpg"  width="136" height="76" alt="" title="web-lankan-clients-super-kids-sri-lanka-b" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-22">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/web-lankan-clients-j-group-of-companies.jpg"  width="136" height="76" alt="web-lankan-clients-j-group-of-companies" title="web-lankan-clients-j-group-of-companies" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-23">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/slt.png"  width="136" height="76" alt="weblankan-client- SLT" title="weblankan-client- SLT" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-24">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/Web-Lankan-Professional-Web-design-in-sri-lanka-4.png"  width="136" height="76" alt="Web-Lankan-Professional-Web-design-in-sri-lanka-4" title="Web-Lankan-Professional-Web-design-in-sri-lanka-4" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-25">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/Etisalat-1.png"  width="136" height="76" alt="weblankan-client- Etisalat" title="weblankan-client- Etisalat" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-26">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/Web-Lankan-Professional-Web-design-in-sri-lanka-6.png"  width="136" height="76" alt="Web-Lankan-Professional-Web-design-in-sri-lanka-6" title="Web-Lankan-Professional-Web-design-in-sri-lanka-6" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-27">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/Web-Lankan-Professional-Web-design-in-sri-lanka-8.png"  width="136" height="76" alt="Web-Lankan-Professional-Web-design-in-sri-lanka-8" title="Web-Lankan-Professional-Web-design-in-sri-lanka-8" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-28">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/09/web-lankan-clients-super-kids-sri-lanka-c.jpg"  width="136" height="76" alt="" title="web-lankan-clients-super-kids-sri-lanka-c" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-29">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/Web-Lankan-Professional-Web-design-in-sri-lanka-10.png"  width="136" height="76" alt="Web-Lankan-Professional-Web-design-in-sri-lanka-10" title="Web-Lankan-Professional-Web-design-in-sri-lanka-10" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-30">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/Web-Lankan-Professional-Web-design-in-sri-lanka-11.png"  width="136" height="76" alt="Web-Lankan-Professional-Web-design-in-sri-lanka-11" title="Web-Lankan-Professional-Web-design-in-sri-lanka-11" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-31">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/09/web-lankan-clients-super-kids-sri-lanka-d.jpg"  width="136" height="76" alt="" title="web-lankan-clients-super-kids-sri-lanka-d" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-32">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/Web-Lankan-Professional-Web-design-in-sri-lanka-13.png"  width="136" height="76" alt="Web-Lankan-Professional-Web-design-in-sri-lanka-13" title="Web-Lankan-Professional-Web-design-in-sri-lanka-13" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-33">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/Web-Lankan-Professional-Web-design-in-sri-lanka-14.png"  width="136" height="76" alt="Web-Lankan-Professional-Web-design-in-sri-lanka-14" title="Web-Lankan-Professional-Web-design-in-sri-lanka-14" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-34">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/Web-Lankan-Professional-Web-design-in-sri-lanka-15.png"  width="136" height="76" alt="Web-Lankan-Professional-Web-design-in-sri-lanka-15" title="Web-Lankan-Professional-Web-design-in-sri-lanka-15" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-35">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/wickramarachchi.png"  width="136" height="76" alt="wickramarachchi" title="wickramarachchi" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-36">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/09/web-lankan-clients-super-kids-sri-lanka-e.jpg"  width="136" height="76" alt="" title="web-lankan-clients-super-kids-sri-lanka-e" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-37">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/Web-Lankan-Professional-Web-design-in-sri-lanka-19.png"  width="136" height="76" alt="Web-Lankan-Professional-Web-design-in-sri-lanka-19" title="Web-Lankan-Professional-Web-design-in-sri-lanka-19" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-38">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/09/web-lankan-clients-super-kids-sri-lanka-f.jpg"  width="136" height="76" alt="" title="web-lankan-clients-super-kids-sri-lanka-f" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-39">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/Web-Lankan-Professional-Web-design-in-sri-lanka-21.png"  width="136" height="76" alt="Web-Lankan-Professional-Web-design-in-sri-lanka-21" title="Web-Lankan-Professional-Web-design-in-sri-lanka-21" />						</div>





					</div>
				</div><!-- /.grid-ibx__item -->

	</div><!-- /.grid-ibx__row -->
	</div>
</div><!-- /.grid-ibx -->

				</div>
			</div>


		</div>

				</div>
			</div>

					</section>


				<section class="zn_section eluidff5619b2     section-sidemargins    section--no " id="eluidff5619b2"  >


			<div class="zn_section_size container zn-section-height--auto zn-section-content_algn--top ">

				<div class="row ">

		<div class="eluidac488049      col-md-12 col-sm-12   znColumnElement"  id="eluidac488049" >


			<div class="znColumnElement-innerWrapper-eluidac488049 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="row elm-testimonial-fader tst-fader eluidfe7ac189  tstfd--light element-scheme--light" ><div class="col-sm-12"><ul class="tst-fader-list  js-slick" data-slick='{"infinite":true,"slidesToShow":1,"slidesToScroll":1,"autoplay":true,"autoplaySpeed":"5000","arrows":null,"appendArrows":".eluidfe7ac189 .tst-fader-controls","dots":false,"fade":true}' ><li class="tst-fader-item"><blockquote class="tst-fader-bqt">We were looking for a quality website, yet simple to use and mobile friendly. Web Lankan understood our requirements and delivered a professionally done and eye appealing website which works great with mobile devices. We tried it on an iPhone and Android as well and it was great on both. We would highly recommend.</blockquote><div class="testimonial-author tst-fader-author clearfix"><div class="testimonial-author--photo tst-fader-photo"><img class="tst-fader-img" src="/wp-content/uploads/2018/07/Mr.-Ushitha-General-30x30_c.png" width="30" height="30" alt="Mr.-Ushitha,-General Manager" title="Mr.-Ushitha,-General Manager"></div><h6 class="tst-fader-author-title">Mr. Ushitha, General Manager | CameraLK</h6></div></li><li class="tst-fader-item"><blockquote class="tst-fader-bqt">When you choose Web Lankan, you get a wonderful, professional team with innovative ideas, awesome customer service, and exactly what you're looking for. Web Lankan took the ideas that we had and put them perfectly on the web than we expected. We were totally delighted.</blockquote><div class="testimonial-author tst-fader-author clearfix"><div class="testimonial-author--photo tst-fader-photo"><img class="tst-fader-img" src="/wp-content/uploads/2018/07/Mr.-Bandhula-Blackpool-Hotel-30x30_c.png" width="30" height="30" alt="Mr.-Bandhula-Blackpool-Hotel" title="Mr.-Bandhula-Blackpool-Hotel"></div><h6 class="tst-fader-author-title"> Mr. Bandhula  | Blackpool Hotel</h6></div></li><li class="tst-fader-item"><blockquote class="tst-fader-bqt">Web Lankan was able to build the complete online marketing and branding concept from the ground up with collaboration with & sync with the offline marketing & branding strategy beyond we expected, ultimately delivering an eye-catching and mobile responsive website.</blockquote><div class="testimonial-author tst-fader-author clearfix"><div class="testimonial-author--photo tst-fader-photo"><img class="tst-fader-img" src="/wp-content/uploads/2018/07/Palitha-athulgama-30x30_c.png" width="30" height="30" alt="Palitha-athulgama" title="Palitha-athulgama"></div><h6 class="tst-fader-author-title">Mr. Palitha Athulgma General Manager | Sierra Cables</h6></div></li></ul></div></div>				</div>
			</div>


		</div>

				</div>
			</div>

					</section>


				<section class="zn_section eluide3b7aab4     section-sidemargins    zn_section--relative section--no " id="eluide3b7aab4"  >

			<div class="zn-bgSource "  ><div class="zn-bgSource-image" style="background-image:url(/wp-content/uploads/2018/06/blackmamba.png);background-repeat:repeat;background-position:center center;background-size:auto;background-attachment:scroll"></div></div>
			<div class="zn_section_size container zn-section-height--auto zn-section-content_algn--top ">

				<div class="row gutter-0">

		<div class="eluid8f4d955e      col-md-12 col-sm-12   znColumnElement"  id="eluid8f4d955e" >


			<div class="znColumnElement-innerWrapper-eluid8f4d955e znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-center znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">
	<div class="zn_custom_container eluid5e647b20  smart-cnt--default   clearfix" >

				<div class="row zn_col_container-smart_container gutter-0">

		<div class="eluid9202f1ca      col-md-8 col-sm-12   znColumnElement"  id="eluid9202f1ca" >


			<div class="znColumnElement-innerWrapper-eluid9202f1ca znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-center znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="kl-title-block clearfix tbk--text- tbk--left text-left tbk-symbol--  tbk-icon-pos--after-title eluid96920681 " ><h3 class="tbk__title" itemprop="headline" >Get a professional website built with a smart and effective strategy ensuring real, measurable results for your business</h3></div><div class="zn-buttonWrapper zn-buttonWrapper-eluid5ca7029e text-left"><a href="/contact-us/" id="eluid5ca7029e" class="eluid5ca7029e  zn-button btn btn-text btn--square btn-icon--before" title="Learn More" target="_self" itemprop="url" ><span class="zn-buttonIcon" data-zniconfam="fontello" data-zn_icon=""></span><span class="zn-buttonText">Learn More</span></a></div>				</div>
			</div>


		</div>

		<div class="eluid510ed92b      col-md-4 col-sm-12   znColumnElement"  id="eluid510ed92b" >


			<div class="znColumnElement-innerWrapper-eluid510ed92b znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="el-videobox eluid3161849d  el-videobox-Type-modal" ><div class="adbox video eluid3161849d  el-videobox-Type-modal" ><img class="adbox-img" src="/wp-content/uploads/2018/07/about-weblankan.jpg"  width="815" height="566" alt="about-weblankan" title="about-weblankan"><div class="video_trigger_wrapper"><div class="adbox_container"><a class="playVideo playvideo-size--md" data-lightbox="iframe" href="//www.youtube.com/watch?v=Ctgg8dKr7P4"></a></div></div></div></div>				</div>
			</div>


		</div>
				</div>
					</div><!-- /.zn_custom_container -->


					</div>
			</div>


		</div>

				</div>
			</div>

					</section>


				<section class="zn_section eluid73e0a0cc     section-sidemargins    section--no " id="eluid73e0a0cc"  >


			<div class="zn_section_size container zn-section-height--custom_height zn-section-content_algn--middle ">

				<div class="row ">

		<div class="eluid8fe9b07f      col-md-7 col-sm-12   znColumnElement"  id="eluid8fe9b07f" >


			<div class="znColumnElement-innerWrapper-eluid8fe9b07f znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-center znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="kl-title-block clearfix tbk--text- tbk--left text-left tbk-symbol--line_border tbk--colored tbk-icon-pos--after-title eluid8a4a92cb " ><h3 class="tbk__title" itemprop="headline" >Welcome to Web Lankan (Pvt) Ltd.</h3><span class="tbk__symbol "><span></span></span><h4 class="tbk__subtitle" itemprop="alternativeHeadline" >We are your strategic digital partner to develop strategies & digital assets that build brands and ultimately increase revenue growth. We provide end-to-end digital solutions to help you maximize the performance of the Website Design & Digital Marketing objectives. <br><br>
As an award-winning Digital Service Provider, we have helped reputed national & international clients to maximize their online marketing attempts and convert visitors to loyal customers.
</h4></div>				</div>
			</div>


		</div>

		<div class="eluid933f910d hidden-lg hidden-md      col-md-12 col-sm-12   znColumnElement"  id="eluid933f910d" >


			<div class="znColumnElement-innerWrapper-eluid933f910d znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-center znColumnElement-innerWrapper--halign-center " >

				<div class="znColumnElement-innerContent">
	<div class="zn_custom_container eluidaf4ca754  smart-cnt--default   clearfix" >

				<div class="row zn_col_container-smart_container ">

		<div class="eluid8926b402      col-md-5 col-sm-5   znColumnElement"  id="eluid8926b402" >


			<div class="znColumnElement-innerWrapper-eluid8926b402 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="image-boxes imgbox-simple eluidd718af8d " ><div class="image-boxes-holder imgboxes-wrapper u-mb-0  "><div class="image-boxes-img-wrapper img-align-left"><img class="image-boxes-img img-responsive " src="/wp-content/uploads/2018/06/weblankan-iso.png"   alt="weblankan-ISO certified"  title="weblankan-ISO certified" /></div></div></div>				</div>
			</div>


		</div>

		<div class="eluid92480d7e      col-md-6 col-sm-6   znColumnElement"  id="eluid92480d7e" >


			<div class="znColumnElement-innerWrapper-eluid92480d7e znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-center znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="image-boxes imgbox-simple eluid49b23f2f " ><div class="image-boxes-holder imgboxes-wrapper u-mb-0  "><div class="image-boxes-img-wrapper img-align-left"><img class="image-boxes-img img-responsive " src="/wp-content/uploads/2018/06/weblankan-sri-lankan-web-design-company-2.png"   alt="weblankan-sri-lankan-web-design-company"  title="weblankan-sri-lankan-web-design-company" /></div></div></div>				</div>
			</div>


		</div>
				</div>
					</div><!-- /.zn_custom_container -->


					</div>
			</div>


		</div>

		<div class="eluid724d1f71      col-md-5 col-sm-12   znColumnElement"  id="eluid724d1f71" >


			<div class="znColumnElement-innerWrapper-eluid724d1f71 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="kl-title-block clearfix tbk--text- tbk--left text-left tbk-symbol--line_border tbk--colored tbk-icon-pos--after-title eluid16bcf6fc " ><h3 class="tbk__title" itemprop="headline" >Member of</h3><span class="tbk__symbol "><span></span></span></div><div class="image-boxes imgbox-simple eluidb957e857 " ><div class="image-boxes-holder imgboxes-wrapper u-mb-0  "><div class="image-boxes-img-wrapper img-align-left"><img class="image-boxes-img img-responsive " src="/wp-content/uploads/2018/06/member-of.png"   alt="web lankan- membership"  title="web lankan- membership" /></div></div></div>				</div>
			</div>


		</div>

		<div class="eluid8d5c97a7 hidden-sm hidden-xs      col-md-12 col-sm-12   znColumnElement"  id="eluid8d5c97a7" >


			<div class="znColumnElement-innerWrapper-eluid8d5c97a7 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-center znColumnElement-innerWrapper--halign-center " >

				<div class="znColumnElement-innerContent">
	<div class="zn_custom_container eluid3b88d0d7  smart-cnt--default   clearfix" >

				<div class="row zn_col_container-smart_container ">

		<div class="eluid54372480      col-md-5 col-sm-5   znColumnElement"  id="eluid54372480" >


			<div class="znColumnElement-innerWrapper-eluid54372480 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="image-boxes imgbox-simple eluid15cf0663 " ><div class="image-boxes-holder imgboxes-wrapper u-mb-0  "><div class="image-boxes-img-wrapper img-align-left"><img class="image-boxes-img img-responsive " src="/wp-content/uploads/2018/06/weblankan-iso.png"   alt="weblankan-ISO certified"  title="weblankan-ISO certified" /></div></div></div>				</div>
			</div>


		</div>

		<div class="eluid9372bafa      col-md-6 col-sm-6   znColumnElement"  id="eluid9372bafa" >


			<div class="znColumnElement-innerWrapper-eluid9372bafa znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-center znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="image-boxes imgbox-simple eluide29784bf " ><div class="image-boxes-holder imgboxes-wrapper u-mb-0  "><div class="image-boxes-img-wrapper img-align-left"><img class="image-boxes-img img-responsive " src="/wp-content/uploads/2018/06/weblankan-sri-lankan-web-design-company-2.png"   alt="weblankan-sri-lankan-web-design-company"  title="weblankan-sri-lankan-web-design-company" /></div></div></div>				</div>
			</div>


		</div>
				</div>
					</div><!-- /.zn_custom_container -->


					</div>
			</div>


		</div>

				</div>
			</div>

					</section>





		<section class="zn_section eluid823882a3     section-sidemargins    section--no " id="eluid823882a3"  >


			<div class="zn_section_size container zn-section-height--auto zn-section-content_algn--top ">

				<div class="row ">

		<div class="eluid99feadc7      col-md-12 col-sm-12   znColumnElement"  id="eluid99feadc7" >


			<div class="znColumnElement-innerWrapper-eluid99feadc7 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="kl-title-block clearfix tbk--text- tbk--center text-center tbk-symbol--line_border tbk--colored tbk-icon-pos--after-title eluid7de41a31 " ><h1 class="tbk__title" itemprop="headline" >Why Choose Our Service</h1><span class="tbk__symbol "><span></span></span></div>
<div class="grid-ibx grid-ibx--cols-4 grid-ibx--md-cols-3 grid-ibx--sm-cols-2 grid-ibx--xs-cols-1 grid-ibx--style-lined-center grid-ibx--hover-bg eluid75fce4d4 why-choose grid-ibx--theme-light element-scheme--light grid-ibx__flt-  " id="eluid75fce4d4">
	<div class="grid-ibx__inner">
		<div class="grid-ibx__row clearfix">
					<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-0">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/07/Trusted-Digital-Partner.png"  width="100" height="100" alt="Trusted-Digital-Partner" title="Trusted-Digital-Partner" />						</div>


					<div class="grid-ibx__title-wrp">
						<h4 class="grid-ibx__title element-scheme__hdg1" itemprop="headline" >Trusted Digital Partner </h4>
					</div>

												<div class="clearfix"></div>
						<div class="grid-ibx__desc-wrp">
							<p class="grid-ibx__desc">The trusted Digital Partner for 300+ reputed national & international brands </p>
						</div>



					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-1">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/07/360-Digital-Solutions.png"  width="100" height="100" alt="360-Digital-Solutions" title="360-Digital-Solutions" />						</div>


					<div class="grid-ibx__title-wrp">
						<h4 class="grid-ibx__title element-scheme__hdg1" itemprop="headline" >360 Digital Solutions </h4>
					</div>

												<div class="clearfix"></div>
						<div class="grid-ibx__desc-wrp">
							<p class="grid-ibx__desc">Specializing full spectrum of digital media to market your business online</p>
						</div>



					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-2">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/07/Professional-Service.png"  width="100" height="100" alt="Professional-Service" title="Professional-Service" />						</div>


					<div class="grid-ibx__title-wrp">
						<h4 class="grid-ibx__title element-scheme__hdg1" itemprop="headline" >Professional Service </h4>
					</div>

												<div class="clearfix"></div>
						<div class="grid-ibx__desc-wrp">
							<p class="grid-ibx__desc">Service of the qualified & experienced professional team of diverse disciplines </p>
						</div>



					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-3">
										<div class="grid-ibx__item-inner">

												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/07/Proactive-Customer-Support.png"  width="100" height="100" alt="Proactive-Customer-Support" title="Proactive-Customer-Support" />						</div>


					<div class="grid-ibx__title-wrp">
						<h4 class="grid-ibx__title element-scheme__hdg1" itemprop="headline" >Proactive Customer Support </h4>
					</div>

												<div class="clearfix"></div>
						<div class="grid-ibx__desc-wrp">
							<p class="grid-ibx__desc">Experience the difference between proactive customer & technical support</p>
						</div>



					</div>
				</div><!-- /.grid-ibx__item -->

	</div><!-- /.grid-ibx__row -->
	</div>
</div><!-- /.grid-ibx -->

				</div>
			</div>


		</div>

				</div>
			</div>

					</section>


				<section class="zn_section eluida430f0fb   zn_parallax  section-sidemargins    zn_section--relative section--no " id="eluida430f0fb"  >


			<div class="zn_section_size full_width zn-section-height--auto zn-section-content_algn--top ">

				<div class="row gutter-0">

		<div class="eluid81f4e0ff hidden-sm hidden-xs      col-md-6 col-sm-12   znColumnElement"  id="eluid81f4e0ff" >


			<div class="znColumnElement-innerWrapper-eluid81f4e0ff znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="kl-title-block clearfix tbk--text- tbk--left text-left tbk-symbol--  tbk-icon-pos--after-title eluida66a19a0 " ><h3 class="tbk__title" itemprop="headline" >Contact us to book a</h3><h4 class="tbk__subtitle" itemprop="alternativeHeadline" >free consultancy session.</h4><div class="tbk__text"><p><span style="color: #ffffff!important;">PHONE : (+94) 11 28 02 280, (+94) 77 88 45 600</span></p>
</div></div>				</div>
			</div>


		</div>

		<div class="eluidfd3250f8      col-md-6 col-sm-12   znColumnElement"  id="eluidfd3250f8" >


			<div class="znColumnElement-innerWrapper-eluidfd3250f8 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="kl-title-block clearfix tbk--text- tbk--left text-left tbk-symbol-- tbk--colored tbk-icon-pos--after-title eluid44fc14ba " ><h1 class="tbk__title" itemprop="headline" >NURTURING DIGITAL PASSION</h1></div>
		<div class="zn_contact_form_container contactForm cf-elm eluid02dec749  cf--light element-scheme--light  " >
						<form action="#" id="form_eluid02dec749" method="post" class="zn_contact_form contact_form cf-elm-form row cf--placeholders" data-redirect=""><div class="col-sm-12  kl-fancy-form zn_form_field zn_cf_text"><input type="text" name="zn_form_field_1_0" id="zn_form_field_1_0" placeholder="Enter Your Email" value="" class="zn_form_input zn-field-text form-control  kl-fancy-form-input zn_validate_is_email "/></div><div class="col-sm-12  kl-fancy-form zn_form_field zn_cf_hidden"><input type="hidden" name="zn_pb_form_submit_1" id="zn_pb_form_submit_1" value="1" class="zn_form_input zn_validate_none" /></div><div class="col-sm-12"><div class="zn_contact_ajax_response titleColor" id="zn_form_id1" ></div><div class="zn_submit_container text-right"><button class="zn_contact_submit btn btn-fullcolor btn--rounded  btn-sm" type="submit">Subscribe Now</button></div></div></form>
		</div>

						</div>
			</div>


		</div>

				</div>
			</div>

					</section>


		</div><div class="znpb-footer-smart-area" >		<section class="zn_section eluidc95ba150  bggradiant-1   section-sidemargins    zn_section--relative section--no " id="eluidc95ba150"  >

			<div class="zn-bgSource "  ><div class="zn-bgSource-overlay" style="background: -webkit-linear-gradient(top,  rgba(53,53,53,0.05) 0%,rgba(53,53,53,0) 100%); background: linear-gradient(to bottom,  rgba(53,53,53,0.05) 0%,rgba(53,53,53,0) 100%); "></div></div>
			<div class="zn_section_size container custom_width_perc zn-section-height--auto zn-section-content_algn--top ">

				<div class="row ">

		<div class="eluid2ed9954a      col-md-3 col-sm-6   znColumnElement"  id="eluid2ed9954a" >


			<div class="znColumnElement-innerWrapper-eluid2ed9954a znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="image-boxes imgbox-simple eluid49a85566 " ><div class="image-boxes-holder imgboxes-wrapper u-mb-0  "><div class="image-boxes-img-wrapper img-align-left"><img class="image-boxes-img img-responsive " src="/wp-content/uploads/2018/06/web-lankan-company.png"   alt=""  title="web-lankan-company" /></div></div></div><div class="kl-title-block clearfix tbk--text- tbk--left text-left tbk-symbol--line_border tbk--colored tbk-icon-pos--after-title eluide67d80b2 " ><h3 class="tbk__title" itemprop="headline" >Sri Lanka</h3><span class="tbk__symbol "><span></span></span></div><div class="znList eluid91949076  text-left znList-icon--left elm-znlist--light element-scheme--light" ><ul class="znListItems clearfix"><li class="znListItems-item clearfix"><span class="znListItems-icon znListItems-icon-0" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">No.75/18, D.L Pathirage Mawatha, Gangodawila, Nugegoda,
Sri Lanka.</span></li><li class="znListItems-item clearfix"><span class="znListItems-icon znListItems-icon-1" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">(+94) 11 28 02 280, (+94) 77 88 45 600</span></li><li class="znListItems-item clearfix"><span class="znListItems-icon znListItems-icon-2" data-zniconfam="glyphicons_halflingsregular" data-zn_icon="✉"></span><span class="znListItems-text">info@weblankan.com</span></li></ul></div><div class="kl-title-block clearfix tbk--text- tbk--left text-left tbk-symbol--line_border tbk--colored tbk-icon-pos--after-title eluidcf033730 " ><h3 class="tbk__title" itemprop="headline" >Australia</h3><span class="tbk__symbol "><span></span></span></div><div class="znList eluid2a770f01  text-left znList-icon--left elm-znlist--light element-scheme--light" ><ul class="znListItems clearfix"><li class="znListItems-item clearfix"><span class="znListItems-icon znListItems-icon-0" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Level 16, 1 Martin Place, Sydney, Australia.</span></li><li class="znListItems-item clearfix"><span class="znListItems-icon znListItems-icon-1" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">+61 400 310 476</span></li></ul></div><div class="image-boxes imgbox-simple eluid897453c1 " ><div class="image-boxes-holder imgboxes-wrapper u-mb-0  "><div class="image-boxes-img-wrapper img-align-left"><img class="image-boxes-img img-responsive " src="/wp-content/uploads/2018/06/country.png"   alt=""  title="country" /></div></div></div><div class="kl-title-block clearfix tbk--text- tbk--left text-left tbk-symbol--line_border tbk--colored tbk-icon-pos--after-title eluid61a14a92 " ><h3 class="tbk__title" itemprop="headline" >ISO 9001:2015 CERTIFIED</h3><span class="tbk__symbol "><span></span></span></div><div class="image-boxes imgbox-simple eluid9044f5c0 footer-img" ><div class="image-boxes-holder imgboxes-wrapper u-mb-0  "><div class="image-boxes-img-wrapper img-align-center"><img class="image-boxes-img img-responsive " src="/wp-content/uploads/2018/06/iso-footer-image.png"   alt="ISO-footer-image"  title="ISO-footer-image" /></div></div></div>				</div>
			</div>


		</div>

		<div class="eluidaa925bc5      col-md-3 col-sm-6   znColumnElement"  id="eluidaa925bc5" >


			<div class="znColumnElement-innerWrapper-eluidaa925bc5 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="kl-title-block clearfix tbk--text- tbk--left text-left tbk-symbol--line_border tbk--colored tbk-icon-pos--after-subtitle eluida267415b " ><h3 class="tbk__title" itemprop="headline" >Web Design & Development</h3><span class="tbk__symbol "><span></span></span></div><div class="znList eluidd91e06a0  text-left znList-icon--left elm-znlist--light element-scheme--light" ><ul class="znListItems clearfix"><li class="znListItems-item clearfix"><a href="/web-design-development/#corporate-website"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-0" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Corporate Website</span></a></li><li class="znListItems-item clearfix"><a href="/web-design-development/#e-commerce-web-store"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-1" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">E commerce Web Store</span></a></li><li class="znListItems-item clearfix"><a href="/web-design-development/#highlight-your-brand-identity"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-2" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Highlight Your Brand Identity</span></a></li><li class="znListItems-item clearfix"><a href="/web-design-development/#SEO"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-3" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">SEO</span></a></li><li class="znListItems-item clearfix"><a href="/web-design-development/#social-media-marketing"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-4" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Social Media Marketing</span></a></li><li class="znListItems-item clearfix"><a href="/web-design-development/#ppc"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-5" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">PPC
</span></a></li><li class="znListItems-item clearfix"><a href="/web-design-development/#online-advertising%20-campaign-consultancy"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-6" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Online Advertising Campaign Consultancy</span></a></li><li class="znListItems-item clearfix"><a href="/web-design-development/#e-mail-marketing"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-7" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">E-mail Marketing</span></a></li><li class="znListItems-item clearfix"><a href="/web-design-development/#website-management-&#038;-maintenance"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-8" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Website Management & Maintenance</span></a></li></ul></div><div class="th-spacer clearfix eluid60d931e0     "></div><div class="kl-title-block clearfix tbk--text- tbk--left text-left tbk-symbol--line_border tbk--colored tbk-icon-pos--after-subtitle eluidee004846 " ><h3 class="tbk__title" itemprop="headline" >E commerce Solutions</h3><span class="tbk__symbol "><span></span></span></div><div class="znList eluid1c27ee78  text-left znList-icon--left elm-znlist--light element-scheme--light" ><ul class="znListItems clearfix"><li class="znListItems-item clearfix"><a href="/ecommerce-solutions/#what-is-e-commerce"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-0" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">What is e-Commerce</span></a></li><li class="znListItems-item clearfix"><a href="/ecommerce-solutions/#what-is-an-e-commerce-website?"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-1" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">What is an E-Commerce website</span></a></li><li class="znListItems-item clearfix"><a href="/ecommerce-solutions/#showcase-your-products-beautifully"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-2" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Showcase Your Products Beautifully</span></a></li><li class="znListItems-item clearfix"><a href="/ecommerce-solutions/#marketing-promotional-and-conversion-tools"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-3" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Marketing, Promotional and Conversion Tools</span></a></li><li class="znListItems-item clearfix"><a href="/ecommerce-solutions/#easy-catalogue-management"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-4" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Easy Catalogue Management</span></a></li><li class="znListItems-item clearfix"><a href="/ecommerce-solutions/#mobile-responsive-ecommerce-website"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-5" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Mobile Responsive Ecommerce Website</span></a></li><li class="znListItems-item clearfix"><a href="/ecommerce-solutions/#SEO-friendly-design-&#038;-development"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-6" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">SEO Friendly Design & Development</span></a></li><li class="znListItems-item clearfix"><a href="/ecommerce-solutions/#user-friendly-&#038;-interactive-pages"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-7" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">User Friendly & Interactive Pages</span></a></li><li class="znListItems-item clearfix"><a href="/ecommerce-solutions/#multilingual-&#038;-currency-conversion"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-8" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Multilingual & Currency Conversion</span></a></li><li class="znListItems-item clearfix"><a href="/ecommerce-solutions/#secure-&#038;-flexible-checkout%20-system"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-9" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Secure & Flexible Checkout System</span></a></li><li class="znListItems-item clearfix"><a href="/ecommerce-solutions/#complete-order-management-system"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-10" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Complete Order Management System</span></a></li><li class="znListItems-item clearfix"><a href="/ecommerce-solutions/#self-site-management-and-cms"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-11" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Self Site Management and CMS</span></a></li><li class="znListItems-item clearfix"><a href="/ecommerce-solutions/#analytics-&#038;-report-system"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-12" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Analytics & Report System</span></a></li><li class="znListItems-item clearfix"><a href="/ecommerce-solutions/#on-line-payment-gateway-providers"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-13" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">On-line Payment Gateway Providers</span></a></li><li class="znListItems-item clearfix"><a href="/ecommerce-solutions/#technologies-&#038;-security"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-14" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Technologies & Security </span></a></li></ul></div><div class="th-spacer clearfix eluidc162d59e     "></div>				</div>
			</div>


		</div>

		<div class="eluid1ad4a042      col-md-3 col-sm-6   znColumnElement"  id="eluid1ad4a042" >


			<div class="znColumnElement-innerWrapper-eluid1ad4a042 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="kl-title-block clearfix tbk--text- tbk--left text-left tbk-symbol--line_border tbk--colored tbk-icon-pos--after-subtitle eluid3781a3ab " ><h3 class="tbk__title" itemprop="headline" >Digital Marketing</h3><span class="tbk__symbol "><span></span></span></div><div class="znList eluidb08fb882  text-left znList-icon--left elm-znlist--light element-scheme--light" ><ul class="znListItems clearfix"><li class="znListItems-item clearfix"><a href="/digital-marketing/#digital-marketing-process"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-0" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Digital Marketing Process</span></a></li><li class="znListItems-item clearfix"><a href="/digital-marketing/#search-engine-optimization"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-1" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Search Engine Optimization</span></a></li><li class="znListItems-item clearfix"><a href="/digital-marketing/#social-media-marketing"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-2" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Social Media Marketing</span></a></li><li class="znListItems-item clearfix"><a href="/digital-marketing/#pay-per-click-advertising"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-3" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Pay-Per-Click Advertising (PPC )</span></a></li><li class="znListItems-item clearfix"><a href="/digital-marketing/#display-advertisement"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-4" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Display Advertisement</span></a></li></ul></div><div class="th-spacer clearfix eluidcb9071b7     "></div><div class="kl-title-block clearfix tbk--text- tbk--left text-left tbk-symbol--line_border tbk--colored tbk-icon-pos--after-subtitle eluid72bd9a42 " ><h3 class="tbk__title" itemprop="headline" >SEO & Analytics</h3><span class="tbk__symbol "><span></span></span></div><div class="znList eluid750c5bae  text-left znList-icon--left elm-znlist--light element-scheme--light" ><ul class="znListItems clearfix"><li class="znListItems-item clearfix"><a href="/seo-analytics/#local-SEO"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-0" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Local SEO</span></a></li><li class="znListItems-item clearfix"><a href="/seo-analytics/#search-engine-marketing"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-1" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Search Engine Marketing</span></a></li><li class="znListItems-item clearfix"><a href="/seo-analytics/#web-analytics"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-2" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Web Analytics</span></a></li></ul></div><div class="th-spacer clearfix eluid8d25b27a     "></div><div class="kl-title-block clearfix tbk--text- tbk--left text-left tbk-symbol--line_border tbk--colored tbk-icon-pos--after-subtitle eluid423e6399 " ><h3 class="tbk__title" itemprop="headline" >Hosting & Domain</h3><span class="tbk__symbol "><span></span></span></div><div class="znList eluid9a57e6bd  text-left znList-icon--left elm-znlist--light element-scheme--light" ><ul class="znListItems clearfix"><li class="znListItems-item clearfix"><a href="/hosting-domain/#domain-name-for-your-business"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-0" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Domain Name for Your Business</span></a></li><li class="znListItems-item clearfix"><a href="/hosting-domain/#business-email"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-1" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Business Email</span></a></li><li class="znListItems-item clearfix"><a href="/hosting-domain/#SSL-certificate"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-2" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">SSL Certificate</span></a></li><li class="znListItems-item clearfix"><a href="/hosting-domain/#cloud-hosting"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-3" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Cloud Hosting</span></a></li><li class="znListItems-item clearfix"><a href="/hosting-domain/#dedicated-server-hosting"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-4" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Dedicated Server Hosting</span></a></li><li class="znListItems-item clearfix"><a href="/hosting-domain/#shared-hosting"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-5" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Shared Hosting</span></a></li></ul></div><div class="th-spacer clearfix eluid8afb27fa     "></div><div class="kl-title-block clearfix tbk--text- tbk--left text-left tbk-symbol--line_border tbk--colored tbk-icon-pos--after-subtitle eluidf34632d2 " ><h3 class="tbk__title" itemprop="headline" >Content Development</h3><span class="tbk__symbol "><span></span></span></div><div class="znList eluid840dee75  text-left znList-icon--left elm-znlist--light element-scheme--light" ><ul class="znListItems clearfix"><li class="znListItems-item clearfix"><a href="/content-development/#content-development-process"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-0" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Content Development Process</span></a></li></ul></div>				</div>
			</div>


		</div>

		<div class="eluid9debaae3      col-md-3 col-sm-6   znColumnElement"  id="eluid9debaae3" >


			<div class="znColumnElement-innerWrapper-eluid9debaae3 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="kl-title-block clearfix tbk--text- tbk--left text-left tbk-symbol--line_border tbk--colored tbk-icon-pos--after-subtitle eluidd9ad1ef5 " ><h3 class="tbk__title" itemprop="headline" >Logo Design  & Branding</h3><span class="tbk__symbol "><span></span></span></div><div class="znList eluidd8b35ba7  text-left znList-icon--left elm-znlist--light element-scheme--light" ><ul class="znListItems clearfix"><li class="znListItems-item clearfix"><a href="/logo-design-branding/#logo-design"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-0" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Logo Design</span></a></li><li class="znListItems-item clearfix"><a href="/logo-design-branding/#complete-branding-solution"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-1" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Complete Branding Solution </span></a></li><li class="znListItems-item clearfix"><a href="/logo-design-branding/#highlight-your-brand-identity"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-2" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Highlight Your Brand Identity</span></a></li></ul></div><div class="th-spacer clearfix eluidd1a9fad1     "></div><div class="kl-title-block clearfix tbk--text- tbk--left text-left tbk-symbol--line_border tbk--colored tbk-icon-pos--after-subtitle eluid23714b86 " ><h3 class="tbk__title" itemprop="headline" >Video / Photography</h3><span class="tbk__symbol "><span></span></span></div><div class="znList eluidd3b63667  text-left znList-icon--left elm-znlist--light element-scheme--light" ><ul class="znListItems clearfix"><li class="znListItems-item clearfix"><a href="/professional-videophotography/#photography-for-websites"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-0" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Photography for Websites</span></a></li><li class="znListItems-item clearfix"><a href="/professional-videophotography/#professional-videography%20"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-1" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Professional Videography </span></a></li><li class="znListItems-item clearfix"><a href="/professional-videophotography/#drone-photography-videography"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-2" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Drone Photography / Videography</span></a></li></ul></div><div class="th-spacer clearfix eluida4d07ea9     "></div><div class="kl-title-block clearfix tbk--text- tbk--left text-left tbk-symbol--line_border tbk--colored tbk-icon-pos--after-subtitle eluid5c08b46f " ><h3 class="tbk__title" itemprop="headline" >Software & Mobile Solutions</h3><span class="tbk__symbol "><span></span></span></div><div class="znList eluid1f6f3f6a  text-left znList-icon--left elm-znlist--light element-scheme--light" ><ul class="znListItems clearfix"><li class="znListItems-item clearfix"><a href="/software-mobile-solutions/#custom-software-development"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-0" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Custom Software Development</span></a></li><li class="znListItems-item clearfix"><a href="/software-mobile-solutions/#mobile-app-development%20"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-1" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Mobile App Development </span></a></li></ul></div><div class="th-spacer clearfix eluidfec3b39f     "></div><div class="kl-title-block clearfix tbk--text- tbk--left text-left tbk-symbol--line_border tbk--colored tbk-icon-pos--after-subtitle eluidb8f68a7d " ><h3 class="tbk__title" itemprop="headline" >Quick Links</h3><span class="tbk__symbol "><span></span></span></div><div class="znList eluidd18525f4  text-left znList-icon--left elm-znlist--light element-scheme--light" ><ul class="znListItems clearfix"><li class="znListItems-item clearfix"><a href="/about-us/"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-0" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">About Web Lankan</span></a></li><li class="znListItems-item clearfix"><a href="/our-works"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-1" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Our Works</span></a></li><li class="znListItems-item clearfix"><a href="/contact-us/"  class="znListItems-link"  target="_self" itemprop="url" ><span class="znListItems-icon znListItems-icon-2" data-zniconfam="glyphicons_halflingsregular" data-zn_icon=""></span><span class="znListItems-text">Contact us</span></a></li></ul></div><div class="th-spacer clearfix eluida680437a     "></div><div class="kl-title-block clearfix tbk--text- tbk--left text-left tbk-symbol--line_border tbk--colored tbk-icon-pos--after-subtitle eluid328707e9 " ><h3 class="tbk__title" itemprop="headline" >Support Tools</h3><span class="tbk__symbol "><span></span></span></div>
<div class="grid-ibx grid-ibx--cols-3 grid-ibx--md-cols-3 grid-ibx--sm-cols-2 grid-ibx--xs-cols-1 grid-ibx--style-lined-center grid-ibx--hover-bg eluidfb6be9df  grid-ibx--theme-light element-scheme--light grid-ibx__flt-  " id="eluidfb6be9df">
	<div class="grid-ibx__inner">
		<div class="grid-ibx__row clearfix">
					<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-0">
										<div class="grid-ibx__item-inner">
						<a href="//www.mozilla.org/en-US/thunderbird/"  class="grid-ibx__link clearfix"  target="_blank" itemprop="url" >
												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/3-1.png"  width="150" height="150" alt="" title="3" />						</div>


					<div class="grid-ibx__title-wrp">
						<h4 class="grid-ibx__title element-scheme__hdg1" itemprop="headline" >Download</h4>
					</div>

												<div class="clearfix"></div>
						<div class="grid-ibx__desc-wrp">
							<p class="grid-ibx__desc">Thunderbird</p>
						</div>


						</a>
					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-1">
										<div class="grid-ibx__item-inner">
						<a href="//www.teamviewer.com/en/"  class="grid-ibx__link clearfix"  target="_blank" itemprop="url" >
												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/2-1.png"  width="150" height="150" alt="" title="2" />						</div>


					<div class="grid-ibx__title-wrp">
						<h4 class="grid-ibx__title element-scheme__hdg1" itemprop="headline" >Download</h4>
					</div>

												<div class="clearfix"></div>
						<div class="grid-ibx__desc-wrp">
							<p class="grid-ibx__desc">TeamViewer</p>
						</div>


						</a>
					</div>
				</div><!-- /.grid-ibx__item -->
							<div class="grid-ibx__item  grid-ibx__item--type-img text-center grid-ibx__item-2">
										<div class="grid-ibx__item-inner">
						<a href="//anydesk.com/download"  class="grid-ibx__link clearfix"  target="_blank" itemprop="url" >
												<div class="grid-ibx__icon-wrp">
						<img class="grid-ibx__icon" src="/wp-content/uploads/2018/06/1-1.png"  width="150" height="150" alt="" title="1" />						</div>


					<div class="grid-ibx__title-wrp">
						<h4 class="grid-ibx__title element-scheme__hdg1" itemprop="headline" >Download</h4>
					</div>

												<div class="clearfix"></div>
						<div class="grid-ibx__desc-wrp">
							<p class="grid-ibx__desc">AnyDesk</p>
						</div>


						</a>
					</div>
				</div><!-- /.grid-ibx__item -->

	</div><!-- /.grid-ibx__row -->
	</div>
</div><!-- /.grid-ibx -->

				</div>
			</div>


		</div>

				</div>
			</div>

					</section>


				<section class="zn_section eluid0481a08b     section-sidemargins    section--no " id="eluid0481a08b"  >


			<div class="zn_section_size full_width zn-section-height--auto zn-section-content_algn--top ">

				<div class="row ">

		<div class="eluidd4264ca5      col-md-6 col-sm-6   znColumnElement"  id="eluidd4264ca5" >


			<div class="znColumnElement-innerWrapper-eluidd4264ca5 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="znList eluid8361743e  text-left znList-icon--left elm-znlist--dark element-scheme--dark" ><ul class="znListItems clearfix"><li class="znListItems-item clearfix"><span class="znListItems-icon znListItems-icon-0" ></span><span class="znListItems-text">Copyright 2018 - Web Lankan (Pvt) Ltd, All Rights Reserved

					</span></li></ul></div>				</div>
			</div>


		</div>

		<div class="eluid7f05a4d6      col-md-6 col-sm-6   znColumnElement"  id="eluid7f05a4d6" >


			<div class="znColumnElement-innerWrapper-eluid7f05a4d6 znColumnElement-innerWrapper znColumnElement-innerWrapper--valign-top znColumnElement-innerWrapper--halign-left " >

				<div class="znColumnElement-innerContent">					<div class="elm-socialicons eluide5b232ca  text-right sc-icon--right elm-socialicons--light element-scheme--light" ><ul class="elm-social-icons sc--custom sh--rounded sc-lay--normal clearfix"><li class="elm-social-icons-item"><a href="//www.facebook.com/weblankan"  class="elm-sc-link elm-sc-icon-0"  target="_blank" itemprop="url" ><span class="elm-sc-icon " data-zniconfam="fontello" data-zn_icon=""></span></a><div class="clearfix"></div></li><li class="elm-social-icons-item"><a href="//www.linkedin.com/company/web-lankan"  class="elm-sc-link elm-sc-icon-1" title="Linkedin" target="_blank" itemprop="url" ><span class="elm-sc-icon " data-zniconfam="fontello" data-zn_icon=""></span></a><div class="clearfix"></div></li></ul></div>				</div>
			</div>


		</div>

				</div>
			</div>

					</section>


		</div></div><!-- end page_wrapper -->

<a href="#" id="totop" class="u-trans-all-2s js-scroll-event" data-forch="300" data-visibleclass="on--totop">TOP</a>

<div class="mobmenu-overlay"></div><div class="mob-menu-header-holder mobmenu"  data-menu-display="mob-menu-slideout-over" ><div class="mob-menu-logo-holder"><a href="" class="headertext"><img class="mob-standard-logo" src="/wp-content/uploads/2017/08/web-lankan-company.png"  alt=" Logo Header Menu"></a></div><div  class="mobmenur-container"><a href="#" class="mobmenu-right-bt" alt="Right Menu Button"><i class="mob-icon-menu mob-menu-icon"></i><i class="mob-icon-cancel mob-cancel-button"></i></a></div></div>				<!--  Right Panel Structure -->
				<div class="mob-menu-right-panel mobmenu mobmenu-parent-link ">
					<a href="#" class="mobmenu-right-bt"  alt="Right Menu Button"><i class="mob-icon-cancel mob-cancel-button"></i></a>
					<div class="mobmenu_content">


		<div class="menu-web-lankan-mobile-top-menu-container"><ul id="mobmenuright"><li  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-2428 current_page_item menu-item-4211 active"><a href="/" class="">Home</a></li><li  class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-4212"><a href="#/" class="">What We Do</a>
<ul  class="sub-menu">
	<li  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4213"><a href="/web-design-development/" class="">Web Design &#038; Development</a></li>	<li  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4214"><a href="/ecommerce-solutions/" class="">eCommerce Solutions</a></li>	<li  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4215"><a href="/digital-marketing/" class="">Digital Marketing</a></li>	<li  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4216"><a href="/seo-analytics/" class="">SEO &#038; Analytics</a></li>	<li  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4217"><a href="/hosting-domain/" class="">Hosting &#038; Domain</a></li>	<li  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4218"><a href="/content-development/" class="">Content Development</a></li>	<li  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4219"><a href="/logo-design-branding/" class="">Logo Design &#038; Branding</a></li>	<li  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4220"><a href="/professional-videophotography/" class="">Professional Video/photography</a></li>	<li  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4221"><a href="/software-mobile-solutions/" class="">Software &#038; Mobile Solutions</a></li></ul>
</li><li  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4222"><a href="/about-us/" class="">Who We Are</a></li><li  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4223"><a href="/our-works/" class="">Our Works</a></li><li  class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4224"><a href="/contact-us/" class="">Contact Us</a></li></ul></div>
			</div><div class="mob-menu-right-bg-holder"></div></div>


	<!-- Fallback for animating in viewport -->
	<noscript>
		<style type="text/css" media="screen">
			.zn-animateInViewport {visibility: visible;}
		</style>
	</noscript>
				<script type="text/javascript">
				function revslider_showDoubleJqueryError(sliderID) {
					var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
					errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
					errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
					errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
					errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
						jQuery(sliderID).show().html(errorMessage);
				}
			</script>
			<script type='text/javascript' src='/wp-content/themes/weblankan/js/plugins.min.js?ver=4.15.2'></script>
<script type='text/javascript' src='/wp-content/themes/weblankan/addons/scrollmagic/scrollmagic.js?ver=4.15.2'></script>
<script type='text/javascript' src='/wp-content/themes/weblankan/framework/zion-builder/assets/js/editor/zn_frontend.js?ver=1.0.1'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var zn_do_login = {"ajaxurl":"\/wp-admin\/admin-ajax.php","add_to_cart_text":"Item Added to cart!"};
var ZnThemeAjax = {"ajaxurl":"\/wp-admin\/admin-ajax.php","zn_back_text":"Back","zn_color_theme":"light","res_menu_trigger":"992","top_offset_tolerance":"","logout_url":"https:\/\/wp-login.php?action=logout&redirect_to=https%3A%2F%&_wpnonce=e323c068b6"};
var ZnSmoothScroll = {"type":"yes","touchpadSupport":"no"};
/* ]]> */
</script>
<script type='text/javascript' src='/wp-content/themes/weblankan/js/znscript.min.js?ver=4.15.2'></script>
<script type='text/javascript' src='/wp-content/themes/weblankan/addons/smooth_scroll/SmoothScroll.min.js?ver=4.15.2'></script>
<script type='text/javascript' src='/wp-content/themes/weblankan/addons/slick/slick.min.js?ver=4.15.2'></script>
<script type='text/javascript' src='/wp-content/plugins/kallyas-addon-nav-overlay/assets/app.min.js?ver=1.0.10'></script>
<script type='text/javascript' src='/wp-includes/js/wp-embed.min.js?ver=4.8.8'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var zn_contact_form = {"captcha_not_filled":"Please complete the Captcha validation"};
/* ]]> */
</script>
<script type='text/javascript' src='//www.google.com/recaptcha/api.js?onload=onloadCallback&#038;render=explicit&#038;hl=en&#038;ver=4.15.2'></script>
<!-- Zn Framework inline JavaScript--><script type="text/javascript">jQuery(document).ready(function($) {
$(document).ready(function() {
	try {
		$('.blue-bggradiant').ripples({
			resolution: 750,
			dropRadius: 10, //px
			perturbance: 0.04,

		});
		$('main').ripples({
			resolution: 128,
			dropRadius: 10, //px
			perturbance: 0.04,
			interactive: false
		});
	}
	catch (e) {
		$('.error').show().text(e);
	}

	$('.js-ripples-disable').on('click', function() {
		$('.blue-bggradiant, main').ripples('destroy');
		$(this).hide();
	});

	$('.js-ripples-play').on('click', function() {
		$('.blue-bggradiant, main').ripples('play');
	});

	$('.js-ripples-pause').on('click', function() {
		$('.blue-bggradiant, main').ripples('pause');
	});

	// Automatic drops
	setInterval(function() {
	    var v = interval_id;//vis()?true:false;
	    console.warn(v);
	    if(v){
	        var $el = $('.blue-bggradiant');
		    var x = Math.random() * $el.outerWidth();
		    var y = Math.random() * $el.outerHeight();
		    var dropRadius = 20;
		    var strength = 0.04 + Math.random() * 0.04;

		    $el.ripples('drop', x, y, dropRadius, strength);
	    }
	}, 5000);


	var interval_id=true;
    $(window).focus(function() {
        interval_id = true;;
    });

    $(window).blur(function() {
        interval_id = false;
    });
});





var $ = jQuery;
function myFunction(x) {
    if (x.matches) { // If media query matches
       $('.blue-bggradiant').attr('class', 'blue');
    } else {

    }
}

var x = window.matchMedia("(max-width: 992px)")
myFunction(x) // Call listener function at run time
x.addListener(myFunction) // Attach listener function on state changes


;(function($) {
			if(navigator.userAgent.match(/(Android|iPod|iPhone|iPad|BlackBerry|IEMobile|Opera Mini)/)){
				$('body').addClass('is-mobile-browser');
				var flipBoxes = $('.znFlipboxElm.znFlipboxElm-pbOff');
				$.each( flipBoxes, function(){
					$(this).on( 'click', function(e) {
						e.preventDefault();
						$(this).toggleClass('is--flipped');
					});
				});
			}
		})(jQuery);
});</script><svg style="position: absolute; width: 0; height: 0; overflow: hidden;" version="1.1" xmlns="//www.w3.org/2000/svg" xmlns:xlink="//www.w3.org/1999/xlink">
<defs>

<symbol id="icon-close-thin" viewBox="0 0 100 100">
 <path d="m87.801 12.801c-1-1-2.6016-1-3.5 0l-33.801 33.699-34.699-34.801c-1-1-2.6016-1-3.5 0-1 1-1 2.6016 0 3.5l34.699 34.801-34.801 34.801c-1 1-1 2.6016 0 3.5 0.5 0.5 1.1016 0.69922 1.8008 0.69922s1.3008-0.19922 1.8008-0.69922l34.801-34.801 33.699 33.699c0.5 0.5 1.1016 0.69922 1.8008 0.69922 0.69922 0 1.3008-0.19922 1.8008-0.69922 1-1 1-2.6016 0-3.5l-33.801-33.699 33.699-33.699c0.89844-1 0.89844-2.6016 0-3.5z"/>
</symbol>


<symbol id="icon-play" viewBox="0 0 22 28">
<path d="M21.625 14.484l-20.75 11.531c-0.484 0.266-0.875 0.031-0.875-0.516v-23c0-0.547 0.391-0.781 0.875-0.516l20.75 11.531c0.484 0.266 0.484 0.703 0 0.969z"></path>
</symbol>

</defs>
</svg>
    <script>
    jQuery(document).ready(function() {

    });
    </script>





<div class="row mobile-number-footer">
			<p style="margin-top:10px;font-size: 24px;"><a href=tel:+94778845600 style="color:#fff;"><span class="znListItems-icon znListItems-icon-3" data-zniconfam="fontello" data-zn_icon=""></span> 077 88 456 00</span></p>
	</div>


<div id="best-web-img"><img src="/wp-content/uploads/2018/07/Web-Lankan-Best-Web-Developer.png" alt="logo" width="150" height="150" border="0" align="right"></div>



</body>
</html>


