<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $clients = Client::orderBy('created_at', 'DESC')
            ->paginate();
        return view('clients.index',['clients'=>$clients]);
    }

    public function create()
    {
        return view('clients.create');
    }

    public function store(\Illuminate\Http\Request $request)
    {
        $client = new Client();
        $client->client_name = $request->client_name;
        $client->client_image = $request->client_image;
        $message = [
            'client_name.required' => 'Please Enter Client Name',
            'client_image.required' => 'Please Choose a Client Logo',
        ];
        $this->validate($request,[

            'client_name' =>'required|max:100',
            'client_image' =>'required',

        ],$message);
        $image = $request->file('client_image');
        $newName = rand() . '.' . $image->getClientOriginalExtension();
        $client->client_image = $newName;
        $client->save();
        $image->move(public_path('images'),$newName);
        return redirect('/admin/client/')->with('status','Project '. $client->client_id .' Created!')->with('path',$newName);

    }


    public function edit($client_id){

        $client = Client::find($client_id);
        return view('clients.edit',compact('client'));
    }

    public function update(\Illuminate\Http\Request $request,$client_id){
        $client = Client::find($client_id);
        $client->client_name = $request->client_name;
        $message = [
            'client_name.required' => 'Please Enter Client Name',

        ];
        $this->validate($request,[
            'client_name' =>'required|max:100',

        ],$message);

        $image = $request->file('client_image');
        if($image){
            $newName = rand() . '.' . $image->getClientOriginalExtension();
            $client->client_image = $newName;
            $image->move(public_path('images/logo'),$newName);
            $client->update();
            return redirect('/admin/client')->with('status','Client '. $client->client_id .' Updated!');
        }
        else{
            $client->client_image;
            $client->update();
            return redirect('/admin/client')->with('status','Client '. $client->client_id .' Updated!');
        }

    }

    public function delete($client_id){

        Client::find($client_id)->delete();
        return redirect('/admin/client/')->with('status','Project '. $client_id .' Deleted!');
    }
}
