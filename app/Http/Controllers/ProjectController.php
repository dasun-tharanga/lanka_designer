<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddNewProject;
use App\Project;
use App\WebsiteImage;
use http\Env\Request;
use Illuminate\Http\Requests;
use Illuminate\Support\Facades\DB;


class ProjectController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $projects = Project::orderBy('created_at', 'DESC')
            ->paginate();
        return view('projects.index',['projects'=>$projects]);
    }

    public function create()
    {
        return view('projects.create');
    }

    public function store(\Illuminate\Http\Request $request)
    {
        $project = new Project();
        $project->website_name = $request->website_name;
        $project->description = $request->description;
        $project->date = $request->date;
        $project->url = $request->url;
        $message = [
            'website_name.required' => 'Please Enter Website Name',
            'description.required' => 'Please Enter Description',
            'url.required' => 'Please Enter URL',
            'date.required' =>'Please Select a Date',
            'website_image.required' => 'Please Select a Website Image',
        ];
        $this->validate($request,[

            'website_name' =>'required|max:100',
            'description' =>'required|max:100',
            'url' =>'required|max:100',
            'date' =>'required',
            'website_image' => 'required|image|mimes:jpeg,png,jpg|max:2048',

        ],$message);
        $image = $request->file('website_image');
        $newName = rand() . '.' . $image->getClientOriginalExtension();
        $project->website_image = $newName;
        $project->save();
        $image->move(public_path('images'),$newName);
        return redirect('/admin')->with('status','Project '. $project->project_id .' Created!')->with('path',$newName);

    }


    public function edit($project_id){

        $project = Project::find($project_id);
        return view('projects.edit',compact('project'));
    }

    public function update(\Illuminate\Http\Request $request,$project_id){
        $project = Project::find($project_id);
        $project->website_name = $request->website_name;
        $project->description = $request->description;
        $project->date = $request->date;
        $project->url = $request->url;
        $message = [
            'website_name.required' => 'Please Enter Website Name',
            'description.required' => 'Please Enter Description',
            'url.required' => 'Please Enter URL',
            'date.required' =>'Please Select a Date',


        ];
        $this->validate($request,[

            'website_name' =>'required|max:100',
            'description' =>'required|max:100',
            'url' =>'required|max:100',
            'date' =>'required',


        ],$message);

        $image = $request->file('update_website_image');
        if($image){
            $newName = rand() . '.' . $image->getClientOriginalExtension();
            $project->website_image = $newName;
            $image->move(public_path('images/website_images'),$newName);
            $project->update();
            return redirect('/admin')->with('status','Project '. $project->project_id .' Updated!');
        }
        else{
            $project->website_image;
            $project->update();
            return redirect('/admin')->with('status','Project '. $project->project_id .' Updated!');
        }

    }

    public function delete($project_id){

      Project::find($project_id)->delete();
      return redirect('/admin')->with('status','Project '. $project_id .' Deleted!');
    }
}
