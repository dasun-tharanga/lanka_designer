<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public $primaryKey = 'project_id';
    public $fillable = ['date','image_id','website_name','description','url'];
    public $timestamps = TRUE;
}
