<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    public $primaryKey = 'client_id';
    public $fillable = ['client_name','client_image'];
    public $timestamps = TRUE;
}
