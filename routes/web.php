<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin','ProjectController@index');
Route::get('/admin/client','ClientController@index');

Route::get('/admin/project/create','ProjectController@create');
Route::get('/admin/client/create','ClientController@create');

Route::resource('project','ProjectController');
Route::resource('client','ClientController');

Route::get('/project/{project_id}/edit','ProjectController@edit')->name('edit');
Route::get('/client/{client_id}/edit','ClientController@edit')->name('editClient');

Route::get('/project/{project_id}/delete','ProjectController@delete')->name('delete');
Route::get('/client/{client_id}/delete','ClientController@delete')->name('deleteClient');




